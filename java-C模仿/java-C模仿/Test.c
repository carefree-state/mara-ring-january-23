#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

int A(int a, int b)
{
	return a + b;
}

struct Kala
{
	int(*p)(int, int);
};
int main()
{
	struct Kala kala = { A };
	printf("%d ", kala.p(5, 6));
}