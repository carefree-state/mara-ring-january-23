import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-07
 * Time: 12:11
 */
public class ArrAys {
    public static void main(String[] args) {
        int[] array1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int[] array2 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        System.out.println(array2 == array1);//不能这么比较，因为这样比较的是引用的地址！！！，那肯定不同啊
        System.out.println(Arrays.equals(array2,array1));//equals,数组比较！
        System.out.println("-----------------------------------");
        int[] array3 = new int[10];
        Arrays.fill(array3,-1); //java中一般要么是功能型方法，要么是返回型方法，但是允许有即返回又功能的方法
        System.out.println(Arrays.toString(array3));
        Arrays.fill(array3,2,4,996);//————》[2,4)，（java中有重载）
        //功能不同的重载
        System.out.println(Arrays.toString(array3));
    }
    public static String shift(char[] str) {
        String ret = "";
        for(char x : str) {
            if(x != ' ') {
                ret += String.valueOf(x);
            }else {
                ret += "%20";
            }
        }
        return ret;
    }
    public static void main7(String[] args) {
        char[] str = { ' ', ' ', ' '};
        System.out.println(shift(str));
    }
    public static void main6(String[] args) {
        int[] array1 = {1, 2, 3, 4, 5, 6, 7, 8};
        int[] ret = Arrays.copyOf(array1,array1.length);//这个方法对应有重载，所以是很多方法
        array1 = Arrays.copyOf(array1,array1.length * 2);
        //这有点功能像C中的realloc 但是不同的是，它必然是会建立的空间必然与原来不同，赋值本身的话，原来那一块销毁
        //realloc只有延申时无完整空间的时候会这么做，并且，原空间必然销毁，与这里有所不同
        //ret = array1 是赋值，不是拷贝，本质上还是引用同一块空间
        System.out.println("--------------------------------");
        System.out.println(Arrays.toString(ret));
        System.out.println(Arrays.toString(array1));
        array1 = Arrays.copyOf(array1,array1.length - 10);
        System.out.println(Arrays.toString(ret));
        System.out.println(Arrays.toString(array1));
        System.out.println("--------------------------------");
        int[] array3 = {1, 2, 3 ,99 ,100};
        int[] copy = new int[array3.length];
        //系统给的拷贝方式，这个可以控制“怎么”拷贝，从哪到哪区间，没有新建立空间，copy必须先建立好
        System.arraycopy(array3,3,copy,0,array3.length - 3);//无返回值
        System.out.println(Arrays.toString(array3));
        System.out.println(Arrays.toString(copy));
        System.out.println("--------------------------------");
        int[] array4 = {1, 2, 3 ,99 ,100};
        int[] copy1 = Arrays.copyOfRange(array4,3,5); //java中的from 到 to，表示左闭右开 ——》[3,5)
        System.out.println(Arrays.toString(array4));
        System.out.println(Arrays.toString(copy1));
    }
    public static String myToString(int[] array) {
        if(array == null) {
            return "null";
        }
        String str = "[";
        for (int i = 0; i < array.length; i++) {
            if(i > 0) {
                str += ", ";
            }
            str += array[i];
        }str += "]";
        return str;
    }
    public static String myToString(double[] array) {
        if(array == null) {
            return "null";
        }
        String str = "[";
        for (int i = 0; i < array.length; i++) {
            if(i > 0) {
                str += ", ";
            }
            str += array[i];
        }str += "]";
        return str;
    }
    public static void main5(String[] args) {
        int[] array1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        double[] array2 = {1.1, 2.2, 3.3, 4.4, 5.5, 6.6, 7.7, 8.8, 9.9, 10.0};
        int[] array3 = null;
        System.out.println(Arrays.toString(array1));
        System.out.println(Arrays.toString(array2));
        System.out.println(Arrays.toString(array3));
        System.out.println(myToString(array1));
        System.out.println(myToString(array2));
        System.out.println(myToString(array3));
    }
    public static int[] grow(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] *= 2;
        }
        return array;
    }
    public static void main4(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int[] array = new int[n];
        for (int i = 0; i < n; i++) {
            array[i] = scan.nextInt();
        }
        int[] ret = grow(array);
        Arrays.sort(ret);
        System.out.println(Arrays.toString(array));
        System.out.println(Arrays.toString(ret));
    }
    public static int[] fib2(int n) {
        if(n <= 0) {
            return null;
        }
        int[] array = new int[n];
        if(n == 1) {
            array[0] = 1;
        }
        if(n >= 2) {
            array[0] = 1;
            array[1] = 1;
        }
        for( int i = 2; i < n; i++) {
            //这里是数组，得到前面的值太容易了！
            //不需要双重循环，每次都自己算出对应的数存再数组里，而是直接通过下标直接得到前面的数就好！
            array[i] = array[i-1] + array[i-2];
        }
        return array;
    }
    //递归实现，每次方法的调用里，建立的堆区不一样，所以没办法做到把第一次建立的堆区的值进行修改，只能修改首两个和最后一个！
    //或者将引用也传过去，但是这样很麻烦
    public static int[] fib1 (int n) {
        if(n <= 0) {
            return null;
        }
        int[] array = new int[n]; //每次方法的调用里，建立的堆区不一样
        if(n == 1) {
            array[0] = 1;
        }
        if(n >= 2) {
            array[0] = 1;
            array[1] = 1;
        }
        if(n > 2) {
            //这里可以借助循环，修改这个递归方法，这还不如直接用循环呢
            for (int i = 3; i <= n; i++) {
                array[i - 1] = fib1(i - 1)[i - 2] +fib1(i - 2)[i - 3];
            }
        }
        return array;
    }
    public static void main3(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        System.out.println(Arrays.toString(fib2(n)));
    }
    public static void main2(String[] args) {
        int[] array1 = { 111, 2, 333, 4, 777};
        func1(array1);//形参改变不了实参！
        System.out.println(Arrays.toString(array1));
        func2(array1);
        System.out.println(Arrays.toString(array1));
    }
    public static void func1(int[] array) {
        array = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    }
    public static void func2(int[] array) {
        array[0] = 999;
    }
    public static void main1(String[] args) {
        //局部变量使用前必须初始化！！！
        int[] arrays1 = null; //不能使用这个引用！这个引用不指向任何对象
        //其实java没有数组和指针的概念，而是融合为 引用
        int[] arrays2 = { 1, 2, 3, 4, 5};
        char[] arrays3 = new char[5];//不能赋值给不同的引用类型
        arrays1 = arrays2;
        System.out.println(arrays1[2]);
        int a = 0;
        arrays2[0] = 100;
        System.out.println(Arrays.toString(arrays1));
        System.out.println(Arrays.toString(arrays2));
        int[] arrays4 = { 6, 7, 8, 9, 10};
        arrays2 = arrays4;//此语句后（只能通过改变上面的代码），无法再找到的
        //无法找到的那个引用指向的内容，会被系统自动回收（无须C语言中的free）
    }
}
