/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-12
 * Time: 18:13
 */
import java.util.Scanner;

class Mm extends Main {

}
//ok!,java之中，定义的成员或者类，是没有先后顺序的，只有语句和代码块是有先后顺序的
public class Main {


    static int c = 10;

    static {
        Main.c = 20;
        Main.c = 21;//静态可以改变已初始化的

    }
    {
        System.out.println("------------------");
    }



    static Base base = new Base(1, 2);//这也算是一个实例代码块，记住这个规矩吧，因为组合的规矩，在类中把别的类作为成员。如果不实例化的话，这就只是一个定义成员而已，所以最先执行，并且在这里初始化就不能再初始化
    //但是如果在这里实例化了对象的话，那么它将被当作实例代码块，在定义其他成员后再执行
    //也合理吧，因为这里有个构造方法相当于执行了语句，不是单纯的定义了。


    {
        System.out.println(base.getX());
        // int b = 2;//局部性质
        this.a = 2;
        this.b = this.a;
        this.a = 4;//在这里可以对成员变量进行“有意义”的初始化，并且这个成员变量不能已经初始化
        this.xiaokala();
        System.out.println("0.0.0");
    }
    //在以下执行之前，先加载类，静态的东西被加载，然后执行静态代码块
//顺序是，先加载好成员变量和成员方法，之后执行实例代码块
    //最后执行构造方法的语句

    //

    int a;//在这里初始化，就不能在实例代码块初始化了！
    int b;

    public void xiaokala() {
        System.out.println("*-*-*");
    }


    public static void main(String[] args) {
        Main main = new Main();
        System.out.println(main.a);

    }
    public static void main1(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextInt()) {
            int x = scanner.nextInt();
            int y = scanner.nextInt();
            int z = scanner.nextInt();
            Sub sub = new Sub(x, y, z);
            System.out.println(sub.calculate());
        }
    }

}

class Base {

    //Main main = new Main();相互套娃，会堆溢出！！！不像那个构造方法会阻止
    private int x;
    private int y;

    {
        System.out.println("********");
    }
    public Base(int x, int y) {
        this.x = x;
        this.y = y;
        System.out.println("xiaokala");
    }

    public Base(int x) {
        this(1, 2);//不能相互this，会直接报错
        this.x = x;
    }

    //@Override ！！！注解  ---> 重写


    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

}

class Sub extends Base {

    private int z;

    public Sub(int x, int y, int z) {
        super(x, y);
        this.z =z;
    }

    public int getZ() {
        return this.z;
    }

    public int calculate() {
        return super.getX() * super.getY() * this.getZ();
    }

}