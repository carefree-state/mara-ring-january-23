/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-10
 * Time: 18:43
 */
//java 没有全局变量，因为主函数外面包裹着类，所以，在外面定义相当于定义了成员，而不是全局变量，不过也有好处，就是可以实例化多个引用
public class DateUtil {
    //这是一个特殊的方法，方法名与类名一样
    //这是一个构造方法，如果没写，系统自动给一个默认不带参数的啥都没有的构造方法
    //因为你在实例化的时候，---> new DateUtil(),是调用！为什么没报错，就是系统自动给了
    //当然，如果你自己写了，以你自己写的为标准
    public DateUtil() {
        //this(1);
        this(1,2,3);//this()调用的是其他构造方法（重载，名字一样，用this就好）记住语法规矩
        //但是，必须写在最前面！
        //也就是说，一个构造方法只有一次机会调用其他构造方法
        System.out.println("把拉卡拉小希拉");
    }//如果没有这句话，你又自己写了构造方法，就没有不带参数的默认构造方法，所以以后就不能这样:>new DateUtil();去构造
    // 重载了！！！
    public DateUtil(int year, int month, int day) {
        //this()不能形成环！！！
        this.year = year;
        this.month = month;
        this.day = day;
    }
    public DateUtil(int year) {
        this.year = year;
    }
    //这条先是成员之间的分割先
    public int year;
    public int month;
    public int day;
    public void setDate(int year, int month, int day) {
        this.year = year; //this 表示当前对象（当前特定的引用变量调用特定成员方法的对象）！
        this.month = month; //这样编译器就知道谁是成员了！
        day = day;//局部变量名优先，所以这里没给对象赋值，就还是默认值
    }

    public void show() {
        System.out.println(this.year + "年" + this.month + "月" + this.day + "日");
    }

    //为什么成员完全方法一样，但是显示不同信息呢，那是因为有隐藏的参数，
    //this 代表的是调用成员方法的引用变量指向的对象，也就是说，一个引用调用的方法，是有特质的



    public static void main(String[] args) {
        DateUtil date = new DateUtil();//调用完构造方法之后，对象就生成了！

        //这里是 构造方法的规矩，记住这个语法规矩！！不要较真
        //1..调用构造方法，之前，就为对象分配了内存，所以定义时可以可以用this
        //2..调用合适的构造方法，在把对象交给引用之前的一些”修饰“

        DateUtil date1 = new DateUtil();
        DateUtil date2 = new DateUtil(2000, 1, 0); //重载到另一个方法
        //实例化的时候，调用一次，也仅此一次
        //下一次就是不同的对象了
        date.setDate(2003, 10, 13);
        date.show();
        date1.setDate(2004, 8, 8);//初始化方法二,这种方法不是改变默认值，而是传参！
        date1.show();
        date2.show();
    }
}
