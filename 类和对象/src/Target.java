import java.util.Arrays;
//  导 包,特定的类
//import java.util.*;随用随取，但是如果这个包这样弄了，有一些类和其他重合了，就会出错
import static java.lang.Math.*; // ”分号注意哦“  这里将类的静态方法导入，就可以直接使用方法了 （lang，的类不需要import）
//import static!!!!!!!


/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-07
 * Time: 22:10
 */


class WashMachine extends  Person{
    public String brand; // 品牌
    public String type; // 型号
    public double weight;

    public WashMachine(String brand, String type, double weight) {
        this.brand = brand;
        this.type = type;
        this.weight = weight;
    }
    public WashMachine(String brand, String type) {
        this(brand,type,0.2);//即使继承了类默认给了super()，也可以用this，第一行就行了！！但是要用带参数的父类构造方法，就必须手写！
        //这样就算一行了！记住这个规矩就好，第一行不包含默认的
    }

    public void washClothes(){ // 洗衣服
        System.out.println("洗衣功能");
    }
    public void dryClothes(){ // 脱水
        System.out.println("脱水功能");
    }
    public void setTime(){ // 定时
        System.out.println("定时功能");
    }
}
class Person {
    public int age;     //年龄，默认为 0
    public boolean value;//boolean 默认为false（0，假）
    public char logo; //char 默认为 ‘/u0000’，/u为前缀，类似于c中的转义字符\x...和\...，代表的是（四位十六进制对应的字符，两个字节）
    public void eat() {
        System.out.println(name + "正在吃饭");
    }
    public void show() {
        System.out.println("name:" + name + " age:" + age);
        System.out.println("value:" + value + " logo:" + logo);
    }
    public String name;//= "A"; //在这里初始化相当于改变了默认值，虽然默认为null（java中null是不指向任何的意思）
    //定义成员就像定义方法一样，没有先后
    public static void main5(String[] args) {
    }
}
//类里面定义成员变量，不要定义类！因为不像C语言可以后面跟着个变量
public class Target {
    public int a = 1;//这是成员！

    public static void main4(String[] args) {
        //Person person1 = new Person();
        //访问修饰符使用在成员变量里，类对象那一块的，public int a = 1;
    }


    //静态方法没法用this
    public static void main(String[] args){
        //在这里，自定义引用类型（的对象（成员变量））有默认的初始化
        //数组也是引用类型，也有默认值
        //String 是引用类型，但是没有初始化，除非它是成员（对象）的时候才会初始化为null
        Person person1 = new Person();
        String str = null; //null小写
        person1.age = 19;
        person1.name = "小卡拉";
        person1.value = true;
        person1.logo = '5';
        System.out.println(person1.age);
        System.out.println(person1.name);
        System.out.println(person1.logo);
        System.out.println(person1.value);
        System.out.println(pow(5,6));
        person1.show();
        //System.out.println(str);错误
    }
    public static void main2(String[] args) {
        int[][] array = {{1, 2, 3}, {2, 4}};
        //这里C语言的指针和数组在Java中结合在一起了
        //Java中没有C那种排列整齐的方正的二维数组，更加诠释了一维数组的数组，这个定义
        for(int[] x : array) {
            for(int y : x) {
                System.out.print(y + " ");
            }
            System.out.println();
        }
        System.out.println(Arrays.deepToString(array));
        //for(int x : array[1]),这种方法只能便利一维数组（一行）
    }
    public static void reverse(int[] arr) {
        int left = 0;
        int right = arr.length - 1;
        while(left < right) {
            int tmp = arr[right];
            arr[right] = arr[left];
            arr[left] = tmp;
            left++;
            right--;
        }
    }
    public static void main1(String[] args) {
        int[] array1 = {1, 2, 4, 5, 6, 7, 1, 2, 10};
        reverse(array1);
        System.out.println(Arrays.toString(array1));
    }
}