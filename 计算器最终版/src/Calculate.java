import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-29
 * Time: 18:55
 */
class PrevNum {
    String string;
    int radix;

    public PrevNum() {
    }

    public PrevNum(String string, int radix) {
        this.string = string;
        this.radix = radix;
    }

    public int getValue() {
        return Integer.parseInt(string, radix);
    }

    public void set(String string, int radix) {
        this.string = string;
        this.radix = radix;
    }

    @Override
    public String toString() {
        if (string == null) {
            throw new 你他妈脑子异常("明明没有上一个");
        }
        return "数值为 '" + string + '\'' +
                ", 进制为 " + radix;
    }
}
public class Calculate {
    public static void cal(Stack<String> stack, String str) {
        String str1 = stack.pop();
        String str2 = stack.pop();
        Double f1 = Double.parseDouble(str1);
        Double f2 = Double.parseDouble(str2);
        switch (str) {
            case "+":
                stack.push(f1 + f2 + "");
                break;
            case "-":
                stack.push(f2 - f1 + "");
                break;
            case "*":
                stack.push(f1 * f2 + "");
                break;
            case "/":
                if(f1 == 0) {
                    throw new ArithmeticException("计算");
                }
                stack.push(f2 / f1 + "");
                break;
            case "%" :
                if(f1 == 0) {
                    throw new ArithmeticException("计算");
                }
                if(((int)(f2 / f1))* f1 == f2) {//确定是否倍数关系的方法
                    stack.push(0 + "");
                }else {
                    stack.push(f2 % f1 + "");
                }
                break;
            case "^":
                stack.push(Math.pow(f2, f1) + "");
                break;
        }
    }
    public static String calculate(String s) {
        List<String> arrayList = reversePolish.parse(s);
        Stack<String> stringStack = new Stack<>();
        for (int i = 0; i < arrayList.size(); i++) {
            String str = arrayList.get(i);
            if(str.equals("+") || str.equals("-") || str.equals("*") || str.equals("/") || str.equals("^") || str.equals("%")) {
                cal(stringStack, str);
            }else {
                stringStack.push(str);
            }
        }
        return stringStack.pop();
    }
    public static void menu() {
        System.out.println("***********###   计算器   ###************");
        System.out.println(" 1. 获取上一次的值继续运算");
        System.out.println(" 2. 重新开始计算");
        System.out.println(" 3. 进制转化（只限整数）");
        System.out.println(" 4. 获取上一次的n进制序列");
        System.out.println(" 5. 查看历史记录");
        System.out.println(" 0. 退出");
        System.out.println("****************************************");
        System.out.print("请选择:> ");
    }

    public static void main(String[] args) {
        String str = "";
        Scanner scanner = new Scanner(System.in);
        PrevNum prevNum = new PrevNum();
        List<String> history = new ArrayList<>();
        int input = 0;
        do {
            menu();
            input = scanner.nextInt();
            scanner.nextLine();
            switch (input) {
                case 1:
                    System.out.print("请输入:> " + str);
                    str += scanner.nextLine();
                    String s1 = str;
                    str = calculate(str);
                    System.out.println("=" + Double.parseDouble(str));
                    history.add(s1 + " = " + Double.parseDouble(str));
                    break;
                case 2:
                    System.out.print("请输入:> ");
                    str = scanner.nextLine();
                    String s2 = str;
                    str = calculate(str);
                    System.out.println("=" + Double.parseDouble(str));
                    history.add(s2 + " = " + Double.parseDouble(str));
                    break;
                case 3:
                    System.out.print("请输入你接下来输入的数的进制:>");
                    int r = scanner.nextInt();
                    scanner.nextLine();
                    System.out.print("请输入整数对应的" + r + "进制数:> ");
                    int number = scanner.nextInt(r);
                    scanner.nextLine();
                    System.out.print("请输入你要转化为进制[2, 36]:> ");
                    int radix = scanner.nextInt();//括号内输入radix,代表输入什么进制的
                    scanner.nextLine();
                    String string1 = Integer.toString(number, radix);
                    System.out.println("转化成功:> " + string1);
                    history.add(Integer.toString(number, r) + " = " + string1 + " (" + r + "->" + radix + ")");
                    prevNum.set(string1, radix);
                    break;
                case 4 :
                    System.out.println(prevNum);
                    System.out.print("请输入你要转化为进制[2, 36]:> ");
                    int newRadix = scanner.nextInt();
                    scanner.nextLine();
                    String string2 = Integer.toString(prevNum.getValue(), newRadix);
                    System.out.println("转化成功:> " + string2);
                    history.add(prevNum.string + " = " + string2 + " (" + prevNum.radix + "->" + newRadix + ")");
                    prevNum.set(string2, newRadix);
                    break;
                case 5:
                    System.out.println("================================================");
                    if(history.size() == 0) {
                        System.out.println("空");
                    }else {
                        for(String s : history) {
                            System.out.println("💖  " + s);
                        }
                    }
                    System.out.println("================================================");
                    break;
                case 0:
                    System.out.println("退出成功");
                    break;
                default:
                    throw new 你他妈脑子异常(" 艹 ");
            }
        }while(input != 0);
    }
}

