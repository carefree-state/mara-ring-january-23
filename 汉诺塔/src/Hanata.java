import java.util.Scanner;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-06
 * Time: 22:32
 */

//递归递归，先递后归，所以递归要眼界放到递的终点，归的起点！
public class Hanata {
    static int count = 1;   //在自己类之中，静态的东西使用可以不加类名. 默认的
    public static void hanaTa(int n, char pos1, char pos2, char pos3){
        if(n == 1) {
            move(pos1, pos3);
            return; //return !!!
        }
        hanaTa(n - 1, pos1, pos3, pos2);
        move(pos1, pos3);
        hanaTa(n - 1, pos2, pos1, pos3);
    }
    public static void move(char pos1, char pos2) {
        System.out.println("第" + count++ + "次移动: " + pos1 + " -> " + pos2 + " ");
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        hanaTa(n,'A','B','C');
    }
}
