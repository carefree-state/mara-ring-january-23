import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;

import java.util.Random;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-08
 * Time: 11:51
 */
public class MiningGame {
    public static void menu() {
        System.out.println("**********************");
        System.out.println("******  1.play  ******");
        System.out.println("******  0.exit  ******");
        System.out.println("**********************");
    }
    public static void printBoard(char[][] show) {
        System.out.println("----------扫雷----------");
        for (int j = 0; j <= show[0].length - 2; j++) {
            System.out.print("--- ");
        }System.out.println();
        for (int i = 0; i <= show[0].length - 2; i++) {
            System.out.print("[" + i +"] ");
        }System.out.println();
        for (int j = 0; j <= show[0].length - 2; j++) {
            System.out.print("--- ");
        }System.out.println();
        for (int i = 1; i <= show.length - 2; i++) {
            System.out.print("[" + i + "] ");
            for (int j = 1; j <= show[0].length - 2 ; j++) {
                System.out.print(" " + show[i][j] +" |");
            }System.out.println();
            for (int j = 0; j <= show[0].length - 2; j++) {
                System.out.print("--- ");
            }System.out.println();
        }
        System.out.println("----------扫雷----------");
    }
    public static void init(char[][] show, char ch) {
        for (int i = 0; i < show.length; i++) {
            for (int j = 0; j < show[0].length; j++) {
                show[i][j] = ch;
            }
        }
    }
    public static void setMine(char[][] mine, int num) {
        Random random = new Random();
        for (int i = 0; i < num; i++) {
            int row = random.nextInt(mine.length - 2) + 1;
            int col = random.nextInt(mine[0].length - 2) + 1;
            if(mine[row][col] == '0') {
                mine[row][col] = '1';
            }else {
                i--;
            }
        }
    }
    public static int starNum(char[][] show) {
        int count = 0;
        for (int i = 1; i <= show.length - 2; i++) {
            for (int j = 1; j <= show[0].length - 2; j++) {
                if(show[i][j] == '*') {
                    count++;
                }
            }
        }
        return count;
    }
    public static int mineNum(char[][] mine, int row, int col) {
        return (
        mine[row + 1][col + 1] + mine[row + 1][col] + mine[row + 1][col - 1] +
        mine[row][col + 1] + mine[row][col - 1] +
        mine[row - 1][col + 1] + mine[row - 1][col] + mine[row - 1][col - 1] - '0' * 8
        );
    }
    public static void exPand(char[][] show, char[][] mine, int row, int col) {
        for (int i = row - 1; i <= row + 1; i++) {
            for (int j = col - 1; j <= col + 1; j++) {
                if( ! (i == row && j == col) ) {
                    if(i < 1 || i > show.length - 2 || j < 1 || j > show[0].length - 2) {
                        continue;
                    }
                    int ret = mineNum(mine, i, j);
                    if(ret == 0 && show[i][j] != ' ') {
                        show[i][j] = ' ';
                        exPand(show, mine, i, j);
                    }if(ret != 0) {
                        show[i][j] = (char)(ret + '0');
                    }
                }
            }
        }
    }
    public static void plaMove(char[][] mine,char[][] show, int num) {
        while(starNum(show) > num ) {
            System.out.print("Please input:>");
            Scanner scan = new Scanner(System.in);
            int row = scan.nextInt();
            int col = scan.nextInt();
            if(row <= show.length - 2 && col >= 1 && row >= 0 && col <= show[0].length - 2) {
                if(show[row][col] != '*') {
                    System.out.println("Repeated");
                    continue;
                }
                if(mine[row][col] == '1') {
                    System.out.println("You are died");
                    printBoard(show);
                    printBoard(mine);
                    break;
                }else {
                    int ret = mineNum(mine, row, col);
                    show[row][col] = (char)(ret + '0');
                    if(ret == 0) {
                        show[row][col] = ' ';
                        exPand(show, mine, row, col);
                    }
                    System.out.println("Success");
                    System.out.println("Stars'num is " + starNum(show));
                    printBoard(show);
                }
            }else {
                System.out.println("Error");
            }
        }
        if(starNum(show) == num) {
            System.out.println("You are won\nSee you again");
        }
    }
    public static void  game(int row, int col) {
        char[][] show = new char[row + 2][col + 2];
        char[][] mine = new char[row + 2][col + 2];
        init(show, '*');
        init(mine, '0');
        int num = 9;    //***********************
        setMine(mine, num);
        printBoard(show);
        plaMove(mine, show, num);
    }
    public static void test() {
        int input = 0;
        int col = 9;    //***********************
        int row = 9;    //***********************
        Scanner scan = new Scanner(System.in);
        do {
            menu();
            System.out.print("Please choose:>");
            input = scan.nextInt();
            switch(input) {
                case 1 :
                    game(col, row);
                    break;
                case 0 :
                    System.out.println("Exit successfully");
                    break;
                default:
                    System.out.println("Error");
                    break;
            }

        }while(input != 0);
    }
}
