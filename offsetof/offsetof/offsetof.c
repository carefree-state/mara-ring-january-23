#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>

//注意结构体引用成员名的时候不能带括号，因为成员名不包括括号的
//这里的0转化为地址是允许的，因为它只是计算过程（不影响原内容的临时拷贝，无副作用）而已，只要最终没有越界访问就好，这里我们最终只是获取地址
//是合法的，但是如果用这个地址去干别的事，就不行，保证不使用，可以看地址可以地址加减，因为地址就是个值！
//我们只是把0看成结构体指针地址，这样就可以用结构体的访问特点，拿到地址，得到结果
//你可能有顾虑是因为0不能直接做地址，但是只要不使用，就没事，可以打印地址，可以地址加减，但是不能看内容并且用内容是干事，例如传参传了数字给指针

#define OFFSETOF(TYPE,MEMBER) (int)&(((TYPE*)0)->MEMBER)

struct stu
{
	char a;
	int b;
	char c;
};

int main()
{
	printf("%d\n", OFFSETOF(struct stu, a));
	printf("%d\n", OFFSETOF(struct stu, b));
	printf("%d\n", OFFSETOF(struct stu, c));
	return 0;
}