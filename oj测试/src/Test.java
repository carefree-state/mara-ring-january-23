import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-24
 * Time: 16:17
 */
class ListNode {
    int val;
    ListNode next = null;

    ListNode(int val) {
        this.val = val;
    }
}
public class Test {

    public boolean IsPopOrder(int [] pushA,int [] popA) {
        Stack<Integer> stack = new Stack<>();
        int j = 0;
        for (int i = 0; i < pushA.length; i++) {
            stack.push(pushA[i]);
            while(!stack.isEmpty() && stack.peek() == popA[j]) {
                stack.pop();
                j++;
            }
            if(j == popA.length) {
                break;
            }
        }
        if(stack.isEmpty()) {
            return true;
        }else {
            return false;
        }
    }


    //6
    public static void main(String[] args) {
        char ch = '√';
        int i = ch;
        char ch2 = 8730;//整形之间不需要明确后缀，规矩罢了只要在字节范围内就好
        System.out.println(i);
        System.out.println(ch2);
    }
    public static void main5(String[] args) {
        String str = "ss";
        double d = 2.2;
        long l = 1;
        //java 中 switch里面可以是字符串 字符 字节 整形 ！！！不能是长整形！！！
        switch (str) {
            case "√":
                break;
            default:
                break;
            case "sss":
                break;
        }
    }


    public static ListNode partition(ListNode pHead, int x) {
        if(pHead == null) {
            return null;
        }
        ListNode fast = pHead.next;
        ListNode prev = pHead;
        ListNode slow = null;
        ListNode cur = null;
        while(fast != null) {

            if(fast.val < x) {
                if(slow == null){
                    slow = fast;
                    cur = slow;
                }else {
                    cur.next = fast;
                    cur = cur.next;
                }
            }else {
                prev.next = fast;
                prev = fast;
            }
            fast = fast.next;
        }
        prev.next = fast;
        if(pHead.val < x) {
            ListNode tmp = pHead;
            pHead = pHead.next;
            tmp.next = slow;
            slow = tmp;
        }
        if(slow == null) {
            slow = pHead;
        }else {
            cur.next = pHead;
        }return slow;
    }


    public static void main4(String[] args) {
        ListNode listNode1 = new ListNode(3);
        listNode1.next = new ListNode(1);
        listNode1.next.next = new ListNode(5);
        listNode1.next.next.next= new ListNode(2);
        listNode1.next.next.next.next = new ListNode(2);

        partition(listNode1, 3);

    }
    public static ListNode mergeTwoLists(ListNode list1, ListNode list2) {
        ListNode head = list1;
        head.next = null;
        ListNode cur1 = list1;
        ListNode cur2 = list2;
        if(list1 == null) {
            return list2;
        }
        if(list2 == null) {
            return list1;
        }
        if(list2.val <= list1.val) {
            cur2.next = head;
            head = cur2;
        }else {
            head.next = cur2;
        }
        cur1 = cur1.next;
        cur2 = cur2.next;
        ListNode cur = head.next;
        while(cur1 != null && cur2 != null) {
            if(cur1.val <= cur2.val) {
                cur.next = cur1;
                cur = cur.next;
                cur1 = cur1.next;
            }else {
                cur.next = cur2;
                cur = cur.next;
                cur2 = cur2.next;
            }
        }
        if(cur1 != null) {
            cur.next = cur1;
        }
        if(cur2 != null) {
            cur.next = cur2;
        }
        return head;
    }
    public static ListNode FindKthToTail(ListNode head,int k) {
        if(head == null) {
            return null;
        }
        ListNode fast = head;
        ListNode slow = head;
        while(fast.next != null) {
            while(fast.next != null && --k != 0) {
                fast = fast.next;
            }
            fast = fast.next;
            if(fast == null) {
                return slow;
            }
            slow = slow.next;
        }
        return slow;
    }

    public static void main3(String[] args) {
        ListNode listNode = new ListNode(1);
        listNode.next = new ListNode(2);
        listNode.next.next = new ListNode(3);
        listNode.next.next.next = new ListNode(4);
        listNode.next.next.next.next = new ListNode(5);
        ListNode listNode1 = FindKthToTail(listNode, 1);
    }

    public static int removeDuplicates(int[] nums) {
        int length = nums.length;
        for(int i = 1; i < length; i++) {
            while(nums[i] == nums[i - 1]) {
                if(i + 1 == length) {
                    length--;
                    break;
                }else {
                    System.arraycopy(nums, i + 1, nums, i, length - i - 1);
                    length--;
                }
            }
        }

        return length;
    }
    public static void merge(int[] nums1, int m, int[] nums2, int n) {
        for(int i = 0; i < n; i++) {
            nums1[i + m] = nums2[i];
        }
        for(int i = 0; i < m + n - 1; i++) {
            for(int j = 1; j < m + n - j - 1; j++) {
                if(nums1[j - 1] > nums1[j]) {
                    int tmp = nums1[j];
                    nums1[j] = nums1[j - 1];
                    nums1[j - 1] = tmp;
                }
            }
        }
    }

    public static void main2(String[] args) {
        int[] nums = {1, 1};
        int length = removeDuplicates(nums);
        for (int i = 0; i < length; i++) {
            System.out.print(nums[i] + " ");
        }
    }
    public static void main1(String[] args) {
        int[] nums1 = {1, 2, 3, 0, 0, 0};
        int[] nums2 = {2, 5, 6};
        merge(nums1, 3, nums2, 3);
        System.out.println(Arrays.toString(nums1));
    }
}
