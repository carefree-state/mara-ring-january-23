import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-06
 * Time: 0:26
 */
public class Circle {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        while(scan.hasNextInt()){//读到的不是int退出
            int n = scan.nextInt();
            System.out.println("n = " + n);
        }
        scan.close();
    }
    public static void main5(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        System.out.println(n);
        scan.close();
    }
    public static void main4(String[] args) {
        System.out.print("%s " + "hhh");
        System.out.println("%s " + "hhh");
        System.out.printf("%s ","hhh");
    }
    public static void main3(String[] args) {
        int i = 1;
        int ret = 0;
        while(i <= 5) {
            int a = 1;
            int sum = 1;
            while (a <= i) {
                sum *= a;
                a++;
            }
            i++;
            ret += sum;
        }
        System.out.println("5的阶乘为" + ret);
        //break 和 continue 跟c语言一样
        //但是没有goto，所以通过 goto 无法跳出多重循环
         i = 1;
         while(i++ <= 100) {
             if(!(i % 3 == 0 && i % 5 == 0)){
                 continue;
             }
             System.out.println(i);
         }
         for(i = 1; i < 10; i++) { //布尔表达式省略默认死循环
             System.out.println(i);
         }


    }




    public static void main2(String[] args) {
        int a = 1;
        int sum = 0;
        while(a <= 100) {
            sum += a;
            a++;
        }
        System.out.println(sum);
        int sum1 = 0;
        int sum2 = 0;
        a = 1;
        while(a <= 100) {
            if(a % 2 == 0) {
                sum1 += a;
            }else {
                sum2 += a;
            }
            a++;
        }
        System.out.println("偶数合是" + sum1);
        System.out.println("奇数合是" + sum2);
    }
    public static void main1(String[] args) {
        int a = 10;
        if(a == 10) {
            System.out.println("a == 10");
        }else {
            System.out.println("a != 10");
        }
        int b = 10;
        if(b == 20) {
            System.out.println("b == 20");
        }else if(b == 0) {
            System.out.println("b == 0");
        }else{
            System.out.println("b->die");
        }

        int c = 10;
        if(a % 2 != 0) {
            System.out.println("奇数");
        }else{
            System.out.println("偶数");
        }

        int year = 2100;
        if((year % 4 ==0 && year % 100 != 0) || year % 400 == 0) {
            System.out.println("是闰年");
        }else{
            System.out.println("不是闰年");
        }

        int s = 10; //不能做switch的参数：long doble float boolean String
        switch (s) {
            case 1:
                System.out.println("1");
                break;
            case 2:
                System.out.println("2");
                break;
            default:
                System.out.println("输入错误！");
                break;
        }
    }
}
