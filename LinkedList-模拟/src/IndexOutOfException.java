/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-28
 * Time: 13:20
 */
public class IndexOutOfException extends RuntimeException{
    public IndexOutOfException() {
    }

    public IndexOutOfException(String message) {
        super(message);
    }
}
