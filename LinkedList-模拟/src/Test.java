import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-28
 * Time: 13:15
 */
public class Test {

    public static void print(MyLinkedList.Node list) {
        if(list == null) {
            System.out.print("[ ");
            return;
        }
        print(list.next);
        System.out.print(list.getValue() + " ");
        if(list.prev == null) {
            System.out.println("]");
        }
    }


    public static void main3(String[] args) {
        MyLinkedList list = new MyLinkedList();
        list.addLast(4);
        list.addLast(2);
        list.addLast(1);
        list.addLast(3);
        list.display(MyLinkedList.sortList(list.head));
    }


    public static void main4(String[] args) {
        MyLinkedList list1 = new MyLinkedList();
        list1.addLast(9);
        MyLinkedList list2 = new MyLinkedList();
        list2.addLast(1);
        list2.addLast(9);
        list2.addLast(9);
        list2.addLast(9);
        list2.addLast(9);
        list2.addLast(9);
        list2.addLast(9);
        list2.addLast(9);
        list2.addLast(9);
        list2.addLast(9);
        MyLinkedList.display(MyLinkedList.addTwoNumbers(list1.head, list2.head));

    }

    public static void main2(String[] args) {
        MyLinkedList myLinkedList = new MyLinkedList();
        myLinkedList.addLast(55);
        myLinkedList.addLast(56);
        myLinkedList.addLast(57);
        myLinkedList.addLast(58);
        myLinkedList.addLast(59);
        print(myLinkedList.head);
        List<Integer> list = new LinkedList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        ListIterator listIterator = list.listIterator(list.size());
        while(listIterator.hasPrevious()) {
            System.out.print(listIterator.previous() + " ");
        }
    }




    public static void main(String[] args) {
        MyLinkedList myLinkedList = new MyLinkedList();
        System.out.println(myLinkedList.size());
        myLinkedList.addLast(155);
        System.out.println(myLinkedList.size());
        myLinkedList.addLast(15);
        System.out.println(myLinkedList.size());
        myLinkedList.addFirst(166);
        System.out.println(myLinkedList.size());
        myLinkedList.addFirst(16);
        System.out.println(myLinkedList.size());
        myLinkedList.addIndex(0, 6);
        System.out.println(myLinkedList.size());
        myLinkedList.addIndex(5, 5);
        myLinkedList.addIndex(1, 7);
        myLinkedList.display();
        System.out.println("=================");
        myLinkedList.clear();
        myLinkedList.addLast(155);
        myLinkedList.addLast(15);
        myLinkedList.addFirst(166);
        myLinkedList.addFirst(16);
        myLinkedList.addIndex(0, 6);
        myLinkedList.addIndex(5, 5);
        myLinkedList.addIndex(1, 7);
        myLinkedList.remove(15);
        myLinkedList.remove(166);
        System.out.println(myLinkedList);
        myLinkedList.clear();
        System.out.println("================");
        myLinkedList.addFirst(55);
        myLinkedList.addFirst(55);
        myLinkedList.addFirst(55);
        myLinkedList.addIndex(2, 555);
        myLinkedList.removeAllKey(55);
        myLinkedList.display();
        System.out.println(myLinkedList.contains(555));
        System.out.println(myLinkedList.contains(55));
    }
}
