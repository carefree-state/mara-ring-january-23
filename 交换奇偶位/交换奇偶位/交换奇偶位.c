#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//会直接作用在N上，因为这并没有建立函数栈帧，不是临时拷贝

#define CHANGE(N)\
int M = 0; \
int i = 0; \
for (i = 0; i < 32; i++)\
{\
if (i % 2 == 0)\
M |= (((N) >> 1) & (1 << i)); \
else \
M |= (((N) << 1) & (1 << i)); \
}\
n=M;

#define C(N) ((0b01010101010101010101010101010101&(N))<<1)+((0b10101010101010101010101010101010&(N))>>1)

//0b.....默认看为无符号补码
//加号优先级大于位操作符（移位操作符大于按位操作符）

int main()
{
	int a = 0b01010101010101010101010101010101;
	int b = 0b10101010101010101010101010101010;
	int n = 0;
	printf("请输入一个数字:>");
	scanf("%d", &n);
	//CHANGE(n);
	printf("%d \n", C(n));
	return 0;
}