/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-14
 * Time: 16:44
 */import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-14
 * Time: 15:46
 */
class Student implements Comparable <Student>{
    public int age;
    public String name;
    public double score;

    public Student(int age, String name, double score) {
        this.age = age;
        this.name = name;
        this.score = score;
    }

    @Override
    public String toString() {
        return "Student{" +
                "age=" + age +
                ", name='" + name + '\'' +
                ", score=" + score +
                '}';
    }

//    @Override
//    public int compareTo(Student o) {
//        return (int)(this.score - o.score);//容易误判！
//    }

//    @Override
//    public int compareTo(Student o) {
//        return String.CASE_INSENSITIVE_ORDER.compare(this.name,o.name);
//    }

    @Override
    public int compareTo(Student o) {
        return this.name.compareTo(o.name);//重载！
    }


//    public int compareTo(Student o) {
//        return this.age - o.age;
//    }
}
class AgeComparator implements Comparator<Student> {
    @Override
    public int compare(Student o1, Student o2) {
        return o1.age - o2.age;
    }
}
public class Demo {
    public static void sort (Comparable[] comparables) { //发生向上转型！！！
        for (int i = 0; i < comparables.length - 1; i++) {
            for (int j = 1; j < comparables.length - i; j++) {
                if(comparables[j - 1].compareTo(comparables[j]) > 0) {
                    Comparable tmp = comparables[j - 1];
                    comparables[j - 1] = comparables[j];
                    comparables[j] = tmp;
                }
            }
        }
    }
    public static void main(String[] args) {
        Student[] student = new Student[3];
        Scanner scanner = new Scanner(System.in);
        student[0] = new Student(19, "c111", 9.9);
        student[1] = new Student(18, "a222", 8.58);
        student[2] = new Student(17, "b333", 7.95);
        // Arrays.sort(student);
        AgeComparator ageComparator = new AgeComparator();
        Arrays.sort(student, ageComparator);//通过这种具体参数决定排序方法，而不是去改变重写方法
        System.out.println(Arrays.toString(student));

        System.out.println("\"+++++++" + '"');// \ 取消特殊含义的字符
        for(Student student1 : student) {
            System.out.println(student1);
        }
    }
}

