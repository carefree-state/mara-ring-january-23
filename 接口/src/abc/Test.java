package abc;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-14
 * Time: 17:21
 */

class Student implements Cloneable {
    public String name;

    public Student(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                '}';
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}


public class Test {
    public static void main(String[] args)throws CloneNotSupportedException {

        Student student = new Student("小卡拉");
        Student student1 = (Student) student.clone();//像数组那样去拷贝！

        System.out.println(student);
        System.out.println(student1);
    }
}
