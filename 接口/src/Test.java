/*
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

*/
/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-14
 * Time: 15:46
 *//*

class Student implements Comparable <Student>{
    public int age;
    public String name;
    public double score;

    public Student(int age, String name, double score) {
        this.age = age;
        this.name = name;
        this.score = score;
    }

    @Override
    public String toString() {
        return "Student{" +
                "age=" + age +
                ", name='" + name + '\'' +
                ", score=" + score +
                '}';
    }

//    @Override
//    public int compareTo(Student o) {
//        return (int)(this.score - o.score);//容易误判！
//    }

//    @Override
//    public int compareTo(Student o) {
//        return String.CASE_INSENSITIVE_ORDER.compare(this.name,o.name);
//    }

    @Override
    public int compareTo(Student o) {
        return this.name.compareTo(o.name);//重载！
    }


//    public int compareTo(Student o) {
//        return this.age - o.age;
//    }
}
public class Test {
    public static void sort (Comparable[] comparables) { //发生向上转型！！！
        for (int i = 0; i < comparables.length - 1; i++) {
            for (int j = 1; j < comparables.length - i; j++) {
                if(comparables[j - 1].compareTo(comparables[j]) > 0) {
                    Comparable tmp = comparables[j - 1];
                    comparables[j - 1] = comparables[j];
                    comparables[j] = tmp;
                }
            }
        }
    }
    public static void main(String[] args) {
        Student[] student = new Student[3];
        Scanner scanner = new Scanner(System.in);
        student[0] = new Student(19, "c", 9.9);
        student[1] = new Student(18, "a", 8.58);
        student[2] = new Student(17, "b", 7.95);
       // Arrays.sort(student);
        sort(student);
        System.out.println(Arrays.toString(student));

        for(Student student1 : student) {
            System.out.println(student1);
        }


    }



}
*/
