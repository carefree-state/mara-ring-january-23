/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-02-01
 * Time: 0:43
 */
public class MyBinaryTree {
    static class TreeNode{
        char val;
        TreeNode left;
        TreeNode right;

        public TreeNode(char val) {
            this.val = val;
        }
    }
    TreeNode root;
    public void create(){
        this.root = new TreeNode('a');
        this.root.left = new TreeNode('b');
        this.root.left.left = new TreeNode('d');
        this.root.left.left.left = new TreeNode('h');
        this.root.left.right = new TreeNode('e');
        this.root.right = new TreeNode('c');
        this.root.right.left = new TreeNode('f');
        this.root.right.right = new TreeNode('g');
    }

}
