import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-20
 * Time: 20:35
 */
public class MyArrayList {

    public int[] elem;
    public int usedSize;//0
    //默认容量
    private static final int DEFAULT_SIZE = 10;

    public MyArrayList() {
        this.elem = new int[DEFAULT_SIZE];
    }

    public MyArrayList(int size) {
        if (size <= 0) {
            throw new IndexException("MyArraylist");
        }
        this.elem = new int[size];
    }

    /**
     * 打印顺序表:
     * 根据usedSize判断即可
     */
    public void display() {
        if(isEmpty()) {
            System.out.println("[]");
        }
        else{
            System.out.print("[ ");
            for (int i = 0; i < this.usedSize; i++) {
                if(i == usedSize - 1) {
                    System.out.print(this.elem[i] + " ]");
                }else {
                    System.out.print(this.elem[i] + ", ");
                }
            }
            System.out.println();
        }

    }

    // 新增元素,默认在数组最后新增
    public void add(int data) {
        if (isFull()) {
            elem = Arrays.copyOf(elem, 2 * usedSize);
        }
        elem[usedSize] = data;
        usedSize++;
    }
    /**
     * 判断当前的顺序表是不是满的！
     *
     * @return true:满   false代表空
     */

    public boolean isFull() {
        if (usedSize == elem.length) {
            return true;
        } else {
            return false;
        }
    }


    private boolean checkPosInAdd(int pos) {
        if (pos < 0 || pos > this.usedSize) {
            return false;
        }
        return true;//合法
    }

    // 在 pos 位置新增元素
    public void add(int pos, int data) {
        if (this.isFull()) {
            elem = Arrays.copyOf(elem, 2 * usedSize);
        }
        if (checkPosInAdd(pos)) {
            usedSize++;
            for (int i = usedSize; i > pos; i--) {
                elem[i] = elem[i - 1];
            }
            elem[pos] = data;
        }else {
            throw new IndexException("add");
        }
    }

    // 判定是否包含某个元素
    public boolean contains(int toFind) {
        for (int i = 0; i < this.usedSize; i++) {
            if (elem[i] == toFind) {
                return true;
            }
        }
        return false;
    }

    // 查找某个元素对应的位置
    public int indexOf(int toFind) {
        for (int i = 0; i < this.usedSize; i++) {
            if (elem[i] == toFind) {
                return i;
            }
        }
        return -1;
    }

    // 获取 pos 位置的元素
    public int get(int pos) {
        if (checkPosInAdd(pos)) {
            if (pos == usedSize) {
                throw new IndexException("get");
            } else {
                return elem[pos];
            }
        } else {
            throw new IndexException("get");
        }
    }

    private boolean isEmpty() {
        return usedSize == 0;
    }

    // 给 pos 位置的元素设为【更新为】 value
    public void set(int pos, int value) {
        if (checkPosInAdd(pos)) {
            if (pos == usedSize) {
                throw new IndexException("set");
            } else {
                this.elem[pos] = value;
            }
        } else {
            throw new IndexException("set");
        }
    }

    /**
     * 删除第一次出现的关键字key
     *
     * @param key
     */
    public void remove(int key) {
        int index = indexOf(key);
        if (index == -1) {
            System.out.println("Can‘t find");
        } else {
            for (int i = index; i < usedSize - 1; i++) {
                this.elem[i] = this.elem[i + 1];
            }
            usedSize--;
        }
    }

    // 获取顺序表长度
    public int size() {
        return usedSize;
    }

    // 清空顺序表
    public void clear() {
        this.elem = new int[DEFAULT_SIZE];
        this.usedSize = 0;
    }
}
