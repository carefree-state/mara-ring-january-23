import jdk.internal.org.objectweb.asm.tree.InnerClassNode;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-20
 * Time: 21:08
 */
public class Test {
    public static void main2(String[] args) {
        MyArrayList myArrayList = new MyArrayList(5);
        myArrayList.add(1, 1);
    }

    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        List<Integer> list1 = new ArrayList<>();
        list.add(1);
        list.add(1);
        list.add(1);
        list.add(1);
        System.out.println(list);
        System.out.println(list.toArray()[0] instanceof Integer);
    }
    public static void main1(String[] args) {
        MyArrayList myArrayList = new MyArrayList(5);
        myArrayList.display();
        myArrayList.add(1);
        myArrayList.add(2);
        myArrayList.add(3);
        myArrayList.display();
        System.out.println(myArrayList.indexOf(3));
        System.out.println(myArrayList.contains(3));
        System.out.println(myArrayList.indexOf(4));
        System.out.println(myArrayList.contains(4));
        myArrayList.add(4);
        myArrayList.add(5);
        myArrayList.add(6);
        myArrayList.display();
        myArrayList.add(2, 99);
        System.out.println(myArrayList.get(2));
        myArrayList.display();
        myArrayList.set(3, 99);
        myArrayList.display();
        myArrayList.remove(4);
        myArrayList.display();
        System.out.println(myArrayList.size());
        myArrayList.clear();
        myArrayList.display();
    }
}
