/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-20
 * Time: 20:45
 */
public class IndexException extends RuntimeException{
    public IndexException() {
    }

    public IndexException(String message) {
        super(message);
    }
}
