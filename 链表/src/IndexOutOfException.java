/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-26
 * Time: 0:32
 */
public class IndexOutOfException extends RuntimeException{
    public IndexOutOfException() {
    }

    public IndexOutOfException(String message) {
        super(message);
    }
}
