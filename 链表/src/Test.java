import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-26
 * Time: 0:01
 */
public class Test {


    public static void main(String[] args) {
        MyLinkedList list = new MyLinkedList();
        list.addLast(1);
        list.addLast(1);
        list.addLast(2);
        list.addLast(3);
        list.addLast(3);
        list.addIndex(0, 4);
        list.addLast(5);
        list.addLast(5);
x        list.display(MyLinkedList.deleteDuplication(list.head));
    }








    public static void main2(String[] args) {
        //两种表都没有输入数据（节点对应值）的构造方法
        List<Integer> list1 = new ArrayList<>(12);//这里输入的是初始容量
        list1.add(5);
        list1.add(56);
        list1.add(15);
        System.out.println(list1);
        List<Integer> list2 = new LinkedList<>(list1);//可以放其他的容器！！！
        System.out.println(list2);
        for(Integer integer : list1) {
            System.out.print(integer + " ");
        }for(Integer integer : list2) {
            System.out.print(integer + " ");
        }
        ListIterator listIterator1 = list1.listIterator();
        ListIterator listIterator2 = list2.listIterator();

    }

    public static void main1(String[] args) {
        MyLinkedList myLinkedList = new MyLinkedList(10);
        myLinkedList.addFirst(11);
        myLinkedList.addFirst(12);
        myLinkedList.addFirst(13);
        myLinkedList.addLast(14);
        myLinkedList.addLast(15);
        myLinkedList.addIndex(9, 16);
        myLinkedList.addIndex(3, 17);
        myLinkedList.display();

        System.out.println("=========================================");

        myLinkedList.clear();
        myLinkedList.display();
        System.out.println(myLinkedList.size());

        System.out.println("=========================================");

        myLinkedList.addFirst(13);
        myLinkedList.addLast(14);
        myLinkedList.addIndex(2, 13);
        myLinkedList.removeAllKey(13);
        System.out.println(myLinkedList);
        myLinkedList.display();
        System.out.println(myLinkedList.size());

        System.out.println("=========================================");

        myLinkedList.addFirst(13);
        myLinkedList.addLast(14);
        myLinkedList.addIndex(2, 13);
        myLinkedList.display();
        myLinkedList.remove(14);
        myLinkedList.display();
        System.out.println(myLinkedList.size());
    }
}
