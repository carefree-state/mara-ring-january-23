import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-21
 * Time: 17:01
 */
class Solution {
    public static List<List<Integer>> generate(int numRows) {
        List<List<Integer>> list = new ArrayList();
        for(int i = 0; i < numRows; i++) {
            list.add((List<Integer>)new ArrayList());
            for(int j = 0; j < i + 1; j++) {
                if(j == 0 || i == j) {
                    list.get(i).add(1);
                }else if(i > 1 && j > 0 && j < i) {
                    list.get(i).add(list.get(i - 1).get(j - 1) + list.get(i - 1).get(j));
                }
            }
        }
        return list;
    }
}
public class Test {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int scan = scanner.nextInt();
        List list = Solution.generate(scan);
        System.out.println(list);
        ArrayList
    }


}
