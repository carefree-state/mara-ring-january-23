import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-06
 * Time: 20:13
 */
public class Test {

    public static void printArray(int[] arrays) {
        for(int x : arrays) {
            System.out.println(x);
        }
    }
    public static void main(String[] args) {
        int[] arrays = { 1, 2, 3, 4, 5};
        printArray(arrays);
    }







    public static void main6(String[] args) {
        int[] arrays = new int[100];
        for (int i = 0; i < arrays.length; i++) {
            arrays[i] = i + 1;
        }
        for(int x : arrays) {
            System.out.print(x + " ");
        }
        //System.out.println((Arrays.toString(arrays)));
    }



    public static int sum(int n) {
        return n == 1 ? 1 : n + sum(n - 1);
    }

    public static void main5(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        System.out.println(sum(n));
    }




    public static void main4(String[] args) {
        int[] array1 = {92,8,3,82,5,17};
        int[] array2 = new int[] {1,2,3,4,5,6};
        int[] array3 = new int[10];
        //for-each
        for(int z : array2) {
            System.out.println(z);
        }
        //Arrays 类方法
        Arrays.sort(array1);
        String ret = Arrays.toString(array1);
        System.out.println(ret);
        System.out.println(Arrays.toString(array2));
        System.out.println(array1);//数组名是引用类型，引用了堆区（元素都开辟在堆区）首元素的地址
        //[  ->数组类型
        //I  ->整型
        //@  ->分割符
        // 后面为八位十六进制数字（没有0x）

        //new 关键字 ->新建一个对象
        //java一切皆对象，
    }


    public static int fib(int n) {
        return n == 1 || n == 2 ? 1 : fib(n - 1) + fib(n - 2);
    }

    public static void main3(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        System.out.println(fib(n));
    }
    public static int breakUp2(int n) {
        return n == 0 ? 0 : breakUp2(n / 10) + n % 10;
    }

    public static void breakUp1(int n) {
        if(n == 0) {
            return;
        }
        breakUp1(n / 10);
        System.out.print(n % 10 + " ");
    }

    public static void main2(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        breakUp1(n);
        System.out.println(breakUp2(n));
    }

    public static int fac(int n) {
        return n >= 2 ? n * fac(n - 1) : 1;
    }
    public static void main1(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        System.out.println(fac(n));
    }
}
