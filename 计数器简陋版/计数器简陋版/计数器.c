#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>
#define N 50
double calculate(char* ps)
{
	int num = 0;
	int count = 1;
	double* arr = calloc(4, 8);
	char* ch = calloc(3, 1);
	int n = 3;
	double* insult = calloc(4 , 8);
	sscanf(ps, "%lf%c%lf", arr, ch, arr + 1);
	while (*(ps + count * 2 + 1) != '\0' && ps[count * 2 + 1] != ' ')
	{
		if (count == n)
		{
			n += 3;
			void* ptr = realloc(arr, n * 8 + 8);
			if (ptr == NULL)
			{
				perror(realloc);
				return 1;
			}
			arr = (double*)ptr;
			memset(arr + n - 2, 0, 32);
			ptr = realloc(insult, n * 8 + 8);
			if (ptr == NULL)
			{
				perror("realloc");
				return 1;
			}
			insult = (double*)ptr;
			memset(insult + n - 2, 0, 32);
			ptr = realloc(ch, n);
			if (ptr == NULL)
			{
				perror("realloc");
				return 1;
			}
			ch = (char*)ptr;
			memset(ch + n - 3, '0', 3);
			ptr = NULL;
		}
		sscanf(ps + count * 2 + 1, "%c%lf", ch + count, arr + count + 1);
		count++;
	}
	for (int i = 0; i < strlen(ch); i++)
	{
		if (i - 1 >= 0 && (ch[i - 1] == '+' || ch[i - 1] == '-') && (ch[i] == '+' || ch[i] == '-'))
		{
			insult[num++] = arr[i];
		}
		if (i == strlen(ch) - 1 && (ch[i] == '+' || ch[i] == '-'))
		{
			insult[num++] = arr[i + 1];
		}
		if (i == 0 && (ch[i] == '+' || ch[i] == '-'))
		{
			insult[num++] = arr[i];
		}
		switch (ch[i])
		{
		case '*':
			insult[num++] = arr[i] * arr[i + 1];
			break;
		case '/':
			insult[num++] = arr[i] / arr[i + 1];
			break;
		case '%':
			insult[num++] = (int)arr[i] % (int)arr[i + 1];
			break;
		case '^':
			insult[num++] = pow(arr[i], arr[i + 1]);
			break;
		default:
			break;
		}
		while (i + 1 < strlen(ch) && (ch[i + 1] == '/' || ch[i + 1] == '*' || ch[i + 1] == '%' || ch[i + 1] == '^') && (ch[i] == '/' || ch[i] == '*' || ch[i] == '%' || ch[i] == '^'))
		{
			i++;
			switch (ch[i])
			{
			case '*':
				num--;
				insult[num] = insult[num] * arr[i + 1];
				num++;
				break;
			case '/':
				num--;
				insult[num] = insult[num - 1] / arr[i + 1];
				num++;
				break;
			case '%':
				num--;
				insult[num] = (int)insult[num - 1] % (int)arr[i + 1];
				num++;
				break;
			case '^':
				num--;
				insult[num] = pow(insult[num - 1], arr[i + 1]);
				num++;
				break;
			default:
				break;
			}
		}
	}
	double sum = insult[0];
	int j = 0;
	for (int i = 1; i < n + 1; i++)
	{
		for (; j < n; j++)
		{
			if (ch[j] == '+')
			{
				sum += insult[i];
				i++;
			}
			if (ch[j] == '-')
			{
				sum -= insult[i];
				i++;
			}
		}
	}
	return sum;
}
int main()
{
	if (1)
	{

	}
	char* ptr = calloc(N, 1);
	if (ptr == NULL)
	{
		perror("calloc");
		return 1;
	}
	char* ps = ptr;
	ptr = NULL;
	int i = 0;
	int count = 1;
	while ((*(ps + i) = getchar()) != '\n')
	{
		if (i == N - 10)
		{
			count++;
			ptr = realloc(ps, count * N);
			memset(ps + (count - 1) * N, '\0', 50);
			if (ptr == NULL)
			{
				perror("realloc");
				return 1;
			}
		}
		i++;
	}
	ps[i] = '\0';
	int c = 1;
	int n = 0;
	double ret = calculate(ps); 
	FILE* pf = fopen("CalHistRecord.txt", "a");
	if (pf == NULL)
	{
		perror("fopen");
		return 1;
	}
	fprintf(pf, "%s=%.3lf\n", ps, ret);
	printf("%s=%.3lf\n", ps, ret);
	fclose(pf);
	pf = NULL;
	free(ps);
	ps = NULL;
	return 0;
}