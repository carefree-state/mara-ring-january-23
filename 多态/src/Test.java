/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-13
 * Time: 14:57
 */




class Dog extends  Animal {
    int a;
    int b;
    static public  void app() {
        //!!! 修饰符没有先后顺序，只有美观与习惯！
    }
    public Dog() {

    }
    public Dog(int a) {
        this.a = a;
    }

    {
        System.out.println("sssssssssssssssssssssssssssssssss");
    }
    public Dog(int a, int b) {
        //Dog(a);
        this(a);//this,只能用this调用其他构造方法！不能用名字，记住这个规矩！！！！！！
        this.b = b;
    }

    @Override
    public void eat() {
        this.show();
        System.out.println("******");
    }
    public void fun() {
        super.eat();//这里可以访问重写之前的父类的eat
    }
}
class Bird extends  Animal {

}


class Animal {

    String name;
    public void show() {
        System.out.println(this);
    }
    public void eat() {
        System.out.println("kacakaca"); //返回值可以不一样，但是只要有一种情况，就是就是父类是父类（其他父类（不一定要与本类相同）也行）返回值，子类是子类返回值，有什么用不重要，了解即可
        //但是不能是其他的！！！！！因为使用情景不一样麻，很好理解，因为你重写之后一般是同一用处的！
        //了解即可！！！！！
    }

}

public class Test {




    public static void main1(String[] args) {
       // Dog dog = new Animal();  这不是向下转型，！！！因为这个构造的时候，没有给子类构造空间，编译错误！
        Animal animal = new Dog(5, 6);
        animal.name = "小卡拉";
        animal.eat(); //多态的原理不是改变！重写也只是语法！多态是通过代码，运行的时候，具体而发生的多种形态！
                        //必须满足规矩就是了！

        //向下转型之前必然有向上转型，并且子类多余的空间并没有被销毁，因为还是有方法访问到
        Dog dog = (Dog) animal; //不安全，但是这就是向下转型，也只能转型为这个，然后将原来的那些多余的空间（因为animal没有办法访问到，但是dog可以）还了回来

        //像这种不会编译错误的，但是运行错误的，（1/0），只要再前面判断一下，不要运行，就不会报错
        if(animal instanceof Bird) {
            Bird bird = (Bird) animal; //animal之前不是bird，这里肯定不能变成bird，就会运行错误（类型出错）,因为原本的开辟的内存没有鸟类的，所以运行时就会发现错误，因为还不回去
        }
        if(animal instanceof Dog) { //instanceof ---判断左边是不是实例化右边的对象
            System.out.println(System.currentTimeMillis());
            System.out.println("----");
            animal.eat();
        }
        //dog变量的，打破了多态的规矩，所以dog调用父类eat的时候，不会多态！
        dog.fun();
        System.out.println(dog.name);
        animal.eat();
    }
}
