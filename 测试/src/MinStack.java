import java.util.Stack;
class Solution {
    public boolean isValid(String s) {
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            if(ch == '(' || ch == '[' || ch == '{') {
                stack.push(ch);
            }else {
                if(!stack.isEmpty()) {
                    char ch1 = stack.peek();
                    if((ch1 == '(' && ch == ')') || (ch1 == '[' && ch == ']') || (ch1 == '{' && ch == '}')) {
                        stack.pop();
                    }else {
                        return false;
                    }
                }else {
                    return false;
                }
            }
        }
        return stack.isEmpty();
    }
    public int evalRPN(String[] tokens) {

        Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < tokens.length; i++) {
            String str = tokens[i];
            if(str.equals("+") || str.equals("-") || str.equals("*") || str.equals("/")) {
                int i2 = stack.pop();
                int i1 = stack.pop();
                switch (str) {
                    case "+":
                        stack.push(i1 + i2);
                        break;
                    case "-":
                        stack.push(i1 - i2);
                        break;
                    case "*":
                        stack.push(i1 * i2);
                        break;
                    case "/":
                        stack.push(i1 / i2);
                        break;
                }
            }else {
                stack.push(Integer.valueOf(str));
            }
        }
        return stack.pop();
    }


    public static boolean IsPopOrder(int [] pushA, int [] popA) {
        Stack<Integer> stack = new Stack<>();
        int j = 0;
        for (int i = 0; i < pushA.length; i++) {
            stack.push(pushA[i]);
            while(!stack.isEmpty() && j < popA.length && stack.peek().equals(popA[j])) {
                stack.pop();
                j++;
            }
        }
        return stack.isEmpty();
    }

    public static void main(String[] args) {
        int[] arr1 = {1};
        int[] arr2 = {1};
        System.out.println(IsPopOrder(arr1, arr2));
    }
}


public class MinStack {
    Stack<Integer> stack1;
    Stack<Integer> stack2;

    public MinStack() {
        this.stack1 = new Stack<>();
        this.stack2 = new Stack<>();
    }
    public void push(int val) {
        if(stack2.isEmpty()) {
            stack2.push(val);
        }
        stack1.push(val);
        if(stack2.peek() >= val) {
            stack2.push(val);
        }
    }
    public void pop() {
        if(stack1.isEmpty()) {
            return;
        }else {
            if(stack1.peek().equals(stack2.peek())) {
                stack2.pop();
            }
            stack1.pop();
        }
    }
    public int top() {
        return stack1.peek();
    }
    public int getMin() {
        return stack2.peek();
    }
}
