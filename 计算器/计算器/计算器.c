#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>
#define N 50
double calculate(char* ps)
{
	int num = 0;
	int count = 1;
	double arr[100] = { 0 };
	char ch[100] = { 0 };
	double insult[100] = { 0 };
	if (*ps == '(')
	{

		insult[num] = calculate(ps + 1);
		arr[0] = insult[num];
		num++;
		count++;
		ps++;
	}
	if (num == 0)
	{
		sscanf(ps, "%lf%c%lf", arr, ch, arr + 1);
	}
	else
	{
		sscanf(ps, "%c%lf", ch + count, arr + count + 1);
	}
	
	while (*(ps + count * 2 + 1) != '\0' && *(ps + count * 2 + 1) != ')' && ps[count * 2 + 1] != '\n')
	{
		sscanf(ps + count * 2 + 1, "%c%lf", ch + count, arr + count + 1);
		count++;
	}
	for (int i = 0; i < strlen(ch); i++)
	{
		if (i - 1 >= 0  && (ch[i - 1] == '+' || ch[i - 1] == '-') && (ch[i] == '+' || ch[i] == '-'))
		{
			insult[num++] = arr[i];
		}
		if (i - 1 >= 0 && i == strlen(ch) - 1 && (ch[i] == '+' || ch[i] == '-'))
		{
			insult[num++] = arr[i + 1];
		}
		if (i == 0 && (ch[i] == '+' || ch[i] == '-'))
		{
			insult[num++] = arr[i];
		}
		switch (ch[i])
		{
		case '*':
			insult[num++] = arr[i] * arr[i + 1];
			break;
		case '/':
			insult[num++] = arr[i] / arr[i + 1];
			break;
		case '%':
			insult[num++] = (int)arr[i] % (int)arr[i + 1];
			break;
		case '^':
			insult[num++] = pow(arr[i], arr[i + 1]);
			break;
		default :
			break;
		}
		while (i + 1 < strlen(ch) && (ch[i + 1] == '/' || ch[i + 1] == '*' || ch[i + 1] == '%' || ch[i + 1] == '^') && (ch[i] == '/' || ch[i] == '*' || ch[i] == '%' || ch[i] == '^'))
		{
			i++;
			switch (ch[i])
			{
			case '*':
				num--;
				insult[num] = insult[num] * arr[i + 1];
				num++;
				break;
			case '/':
				num--;
				insult[num] = insult[num - 1] / arr[i + 1];
				num++;
				break;
			case '%':
				num--;
				insult[num] = (int)insult[num - 1] % (int)arr[i + 1];
				num++;
				break;
			case '^':
				num--;
				insult[num] = pow(insult[num - 1], arr[i + 1]);
				num++;
				break;
			default:
				break;
			}
		}
	}
	double sum = insult[0];
	int j = 0;
	for (int i = 1; i < 10; i++)
	{
		for (; j < strlen(ch); j++)
		{
			if (ch[j] == '+')
			{
				sum += insult[i];
				i++;
			}
			if (ch[j] == '-')
			{
				sum -= insult[i];
				i++;
			}
		}
	}
	if (*(ps + count * 2 + 1) != ')')
	{
	while (*(ps + count * 2 + 2)=='*'|| *(ps + count * 2) == '%'|| *(ps + count * 2 + 2) == '^'|| *(ps + count * 2 + 2) == '/')
	{
		sscanf(ps + count * 2 + 2, "%c%lf", ch + count, arr + count + 1);
		switch (ch[count])
		{
		case '*':
			sum *= arr[count + 1];
			break;
		case '/':
			sum /= arr[count + 1];
			break;
		case '^':
			sum = pow(sum, arr[count + 1]);
			break;
		case '%':
			sum = (int)sum % (int)arr[count + 1];
			break;
		default:
			break;
		}
		count++;
	}
	}
	
	return sum;
}
int main()
{
	char* ptr = calloc(N, 1);
	if (ptr == NULL)
	{
		perror("calloc");
		return 1;
	}
	char* ps = ptr;
	ptr = NULL;
	int i = 0;
	int count = 1;
	while ((*(ps + i) = getchar()) != '\n')
	{
		if (i == N - 1)
		{
			count++;
			ptr = realloc(ps, count * N);
			memset(ps + (count - 1) * N, '\0', 50);
			if (ptr == NULL)
			{
				perror("realloc");
				return 1;
			}
		}
		i++;
	}
	int c = 1;
	int n = 0;
	printf("%lf ",calculate(ps)); ;
	return 0;
}