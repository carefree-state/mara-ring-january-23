/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-05
 * Time: 10:40
 */
public class ThinGain {
    public static void main(String[] args) {
        String str1 = "nowmre!";
        String str2 = "thingain";
        System.out.println(str1+str2);//拼接!
        System.out.println(str1+123);//拼接!
        System.out.println(str1+123+124);//拼接!
        System.out.println(str1+(123+124));//拼接!
        System.out.println(123+124+str1);//拼接!
        System.out.println((123+124)+str1);//拼接!

        int a = 123456;
        String str3 = String.valueOf(a);
        System.out.println(str3);
        float f = 12.5f;
        str3 = String.valueOf(f);
        System.out.println(str3);
        str3 = a + "";
        System.out.println(str3);
        int b = Integer.valueOf(str3);
        System.out.println(b);
        str3 = "12.5";
        float c = Float.valueOf(str3);
        System.out.println(c);
        str3 = "true";
        boolean bb = Boolean.valueOf(str3);
        System.out.println(bb);


        System.out.println(-10 % 3);//-10 -> 3*(-3),相减->-1
        System.out.println(-10 % -3);//-10 -> -3*3,相减->-1
        System.out.println(10 % -3);//10 -> -3*(-3),相减->1
        System.out.println(10 % 3);//->1
        System.out.println(12.5%2);//java中允许小数取模！！！


        short sh = 10 ;
        sh += 32758; //自动类型转化,将结果的类型强制类型转化了！
        //sh = sh +1;错误！！！
        System.out.println(sh);
        System.out.println(Short.MAX_VALUE);


        int i = 10 ;
        i = i++; //----> i++; i = 10; 直接替换数值 i++这个整体的值为10
        i = ++i; //----> ++i; i = 11; 直接替换数值 ++i这个整体的值为11
        System.out.println(i);


        //记住一个东西，Java不允许一个值为一个语句，即使一个值被忽略，比如三目操作符


        //逻辑 的与或非 的时候必须两边都是布尔表达式！！！不然就编译不过！常量表达式或者其他
        //结果都不是boolean！比如1 && true 不行！ -1<0<1 (true < 1),不行！
        //!(布尔表达式),不能 !(1) 等等...
        //如果是&而不是&&，则两边都是布尔表达式（布尔类型与其他类型之间不能比较）
        //则还是逻辑运算，但是没有短路现象

        System.out.println(i);
        System.out.println((true == true ? true == true ? false : true :true));
    }
}
