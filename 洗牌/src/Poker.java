/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-23
 * Time: 18:41
 */
public class Poker {
    char quality;
    String value;

    @Override
    public String toString() {
        return " [" +
                quality +
                value +
                "] ";
    }

    public Poker(char quality, String value) {
        this.quality = quality;
        this.value = value;
    }

    public char getQuality() {
        return quality;
    }

    public void setQuality(char quality) {
        this.quality = quality;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
