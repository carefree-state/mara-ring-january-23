import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-23
 * Time: 18:40
 */
public class Test {
    public static void buy(List<Poker> pokerList, String str1, String str2) {
        for(int i = 0; i < 4; i++) {
            for(int j = 1; j <= 13; j++) {
                char ch = '\u0000';
                String str = "";
                ch = str1.charAt(i);
                str += "" + str2.charAt(2 * j - 2) + str2.charAt(2 * j - 1);
                Poker poker = new Poker(ch, str);
                pokerList.add(poker);
            }
        }
        pokerList.add(new Poker('小', "鬼"));
        pokerList.add(new Poker('大', "鬼"));
    }
    public static void show(List<Poker> pokerList) {
        for (int i = 1; i <= pokerList.size(); i++) {
            System.out.print(pokerList.get(i - 1));
            if(i % 6 == 0) {
                System.out.println();
            }
        }
        System.out.println();
    }
    public static void swap(List<Poker> pokerList, int index1, int index2) {
        Poker tmp = pokerList.get(index1);
        pokerList.set(index1, pokerList.get(index2));
        pokerList.set(index2, tmp);
    }
    public static void disorder(List<Poker> pokerList) {
        for (int i = pokerList.size() - 1; i >= 0 ; i--) {
            Random random = new Random();
            int index = random.nextInt(i + 1);
            swap(pokerList, i, index);
        }
    }
    public static void handOutFive(List<Poker> pokerList, List<List<Poker>> lists, int size) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < size; j++) {
                lists.get(i).add(pokerList.remove(0));
                if(0 == pokerList.size()) {
                    System.out.println("发完了！");
                    return;
                }
            }
        }
    }
    public static void bullfighting(List<List<Poker>> lists) {
        String[][] strings = new String[3][5];
        for (int j = 0; j < 3; j++) {
            int sum = 0;
            System.out.print("第" + (j + 1) + "个人:");
            show(lists.get(j));
            for(int i = 0; i < 5; i++) {
                if(lists.get(j).get(i).getValue().equals("鬼")) {
                    sum += 10;
                    strings[j][i] = "10";
                    continue;
                }
                char ch = lists.get(j).get(i).getValue().charAt(1);
                if(ch >= '1' && ch <= '9') {
                    sum += ch - '0';
                    strings[j][i] = "" + ch;
                }else {
                    sum += 10;
                    strings[j][i] = "10";
                }
            }
            if(sum % 10 == 0) {
                System.out.println("牛牛！");
            }else {
                int count = 0;
                for (int a = 0; a < 5; a++) {
                    for (int b = 0; b < 5; b++) {
                        for (int c = 0; c < 5; c++) {
                            if(a != b && b != c && a != c) {
                                int s = Integer.parseInt(strings[j][a]) + Integer.parseInt(strings[j][b]) + Integer.parseInt(strings[j][c]);
                                if(s % 10 == 0) {
                                    if(count++ == 0) {
                                        System.out.println("牛" + sum % 10 + "!");
                                        a = 5;
                                        b = 5;
                                        c = 5;
                                    }
                                }
                            }
                        }
                    }
                }
                if(count == 0) {
                    System.out.println("无牛!");
                }
            }
        }
    }
    public static void show(List<List<Poker>> lists, String str) {
        for (int i = 0; i < 3; i++) {
            System.out.print("第" + (i + 1) + "个人:");
            show(lists.get(i));
        }
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextLine()) {
            String str = scanner.nextLine();
            List<Poker> pokerList = new ArrayList<>();
            List<Poker> tmp = null;
            String str1 = "♥♠♣♦";
            String str2 = " 1 2 3 4 5 6 7 8 910 J Q K";
            //买牌--------------
            buy(pokerList, str1, str2);
            tmp = pokerList;
            //取牌--------------
            //show(pokerList);
            //洗牌--------------
            disorder(pokerList);
            //show(pokerList);
            //发牌--------------
            List<List<Poker>> lists = new ArrayList<>();
            List<Poker> list1 = new ArrayList<>();
            List<Poker> list2 = new ArrayList<>();
            List<Poker> list3 = new ArrayList<>();
            lists.add(list1);
            lists.add(list2);
            lists.add(list3);
            handOutFive(pokerList, lists, 5);
//            for(List<Poker> list : lists) {
//                show(list);
//            }
            //斗牛
            bullfighting(lists);
//        show(lists, "all");
        }
    }
}
