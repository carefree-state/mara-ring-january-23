import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-20
 * Time: 13:04
 */

class A implements Comparable, Cloneable{
    int a;
    int b;

    @Override
    public int compareTo(Object o) {
        return 0;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}


class Demo1 <T> { //尖括号里面是什么字母不重要，就只是一个代词而已，只不过特殊字母特殊的字面含义罢了
    Object[] objects = new Object[3]; //三个null  元素还没实例化，根据T 向上转型，所以后面的向下转型合理！

    public T getObjects(int pos) {
        return (T)objects[pos];//很危险
    }

    public void setObjects(T t, int pos) {
        this.objects[pos] = t;
    }
}

public class Test {




    public static <E> int  removeElement(int[] nums, int val) {
        int count = nums.length;

        for(int i = 0; i < count; i++) {
            if(nums[i] == val) {
                for(int j = i + 1; j < count; j++) {
                    nums[j - 1] = nums[j];
                }
                count--;
                i = 0;
            }
        }
        return count;
    }
    public static void main(String[] args) {
        int[] arr = {3, 3};
        int count = removeElement(arr, 3);
        for (int i = 0; i < count; i++) {
            System.out.print(arr[i]);
        }
    }



    public static void main3(String[] args)throws CloneNotSupportedException  {
        A a = new A();
        A a1 = (A)a.clone();
    }




    public static void main2(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str1 = scanner.nextLine();
        double d1 = scanner.nextDouble();
        int i1 = scanner.nextInt();
        int i2 = scanner.nextInt();
        int i3 = scanner.nextInt();
        double d2 = scanner.nextDouble();
        scanner.nextLine();//nextLine() ，要吃掉上一个输入遗留的回车，如果上一个是nextLine的话，就不需要！因为回车已经被他吃掉并删掉了
        String str2 = scanner.nextLine();
        System.out.println(str1);
        System.out.println(d1);
        System.out.println(i1);
        System.out.println(d2);
        System.out.println(str2);
    }



    public static void main1(String[] args) {
        Demo1<String> stringDemo1 = new Demo1<>();
        stringDemo1.setObjects("xiaokala", 0);
        stringDemo1.setObjects("xiaopili", 1);
        stringDemo1.setObjects("xiaobala", 2);
        System.out.println(stringDemo1.getObjects(0));
        System.out.println(stringDemo1.getObjects(1));
        System.out.println(stringDemo1.getObjects(2));
    }
}
