import java.util.Arrays;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-16
 * Time: 15:50
 */
interface A {

}
class B {
    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();//学异常就懂了！！！不要着急
    }
}
public class StrMain {


    public static void main(String[] args) {
        String str = "111000440";
        System.out.println(str.contains("44"));// 找到了返回 真
        System.out.println(str.contains("5")); //找不到返回 假
        if(-1 == str.indexOf("5")) { //查找找不到返回-1
            System.out.println(false);
        }
    }












    public static boolean isPalindrome(String s) {
        s = s.toLowerCase();
        StringBuilder stringBuilder = new StringBuilder(s);
        stringBuilder = stringBuilder.reverse();
        String str = stringBuilder.toString();
        int i = 0;
        int j = 0;
        for(; i < s.length() && j < str.length(); i++, j++) {
            while( i < s.length()&&!(s.charAt(i) <= 'z'&& s.charAt(i) >= 'a') && !(s.charAt(i) <= '9'&& s.charAt(i) >= '0')) {
                i++;
            }
            //利用短路先判断！
            while(j < str.length()&&!(str.charAt(j) <= 'z'&& str.charAt(j) >= 'a') && !(str.charAt(j) <= '9'&& str.charAt(j) >= '0') ) {
                j++;
            }
            if(i < s.length()&&j < str.length()&&s.charAt(i) != str.charAt(j)) {
                return false;
            }
        }
        return true;
    }
    public static void main2(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        System.out.println(isPalindrome(str));
    }


    public static void main3(String[] args) {
        String str1 = "111110001111110000";
        char[] str3 = "11111000111111".toCharArray();
        String[] str2 = str1.split("");
        System.out.println(Arrays.toString(str2));
        System.out.println(Arrays.toString(str3));
        str2 = str1.split("0|\\*");//limit小于等于0，则等于没有
        System.out.println(Arrays.toString(str2));
        str2 = str1.split("0000");
        System.out.println(Arrays.toString(str2));
        StringBuilder stringBuilder = new StringBuilder();
        //stringBuffer 几乎跟以上那个是用法一样的，但是本质有区别，链程多的，用buffer锁上更好
        //String str = (String)stringBuilder;不能将这个强制类型转化
        stringBuilder.append(5).append(1.5);
        System.out.println(stringBuilder);
        char ch = 0;
        String str = stringBuilder.toString();
        //StringBuilder有专门的toString（）方法！（因为打印的时候，它重写了toString（）方法）
        System.out.println(str);
    }


    public static void main1(String[] args) {
        char[] chars = {'1', '2', '3'};
        int[] ints = {1, 2, 3};
        String str1 = null;
        String str2 = "";
        //str1 += "111"; 这个结果是    null111
        str2 += "111";
        System.out.println(chars);
        System.out.println(chars.length);
        System.out.println(ints);
        System.out.println(str1);
       // System.out.println(str1.isEmpty()); 空指针异常
        //System.out.println(str1.length());
        System.out.println(str2);
        System.out.println(str2.length());
        System.out.println(str2.isEmpty());
        String str3 = "ss";
        String str4 = "ss";
        System.out.println(str3.charAt(1));
        str4 += 1;
        String str5 = new String("ss");
        System.out.println("ss" == str4);//只能说两个对象指向同一位置
        System.out.println(str3 == str5);
        System.out.println(str3.equals(str5));
        System.out.println("ss1".equals(str4));
        int a = Integer.valueOf("123");
        a = Integer.parseInt("122");
        System.out.println(1 + a);
    }
}
