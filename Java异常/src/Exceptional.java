import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-17
 * Time: 20:23
 */




class A implements Cloneable{ //Cloneable 有特殊之处！
    int a;
    int b;

    public A(int a, int b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public String toString() {
        return "A{" +
                "a=" + a +
                ", b=" + b +
                '}';
    }

    @Override
    protected Object clone() throws CloneNotSupportedException{
        return super.clone();
    }
}

interface B {

}



class NameException extends RuntimeException {
    public NameException() {
    }

    public NameException(String message) {
        super(message);
    }
}
class PasswordException extends RuntimeException {
    public PasswordException() {
    }

    public PasswordException(String message) {
        super(message);
    }
}

public class Exceptional {
    public static void loginName(String str) {
        if(!str.equals("小卡拉")) {
            throw new NameException("用户名错误");
        }else {
            System.out.print("请输入密码:>");
        }
    }
    public static void loginPassword(String str) {
        if(!str.equals("mmsszsd")) {
            throw new PasswordException("密码错误");
        }else {
            System.out.println("登录成功！");
        }
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int count = 3;
        String name = null;
        String password = null;
        while(count-- != 0) {
            try {
                System.out.print("请输入用户名:>");
                name = scanner.nextLine();
                loginName(name);
                password = scanner.nextLine();
                loginPassword(password);
                if(password.equals("mmsszsd") && name.equals("小卡拉")) {
                    break;
                }
            }catch (NameException e) {
                e.printStackTrace();
                System.out.println("NameException 异常 必须处理");
            }catch (PasswordException e) {
                e.printStackTrace();
                System.out.println("PasswordException 异常 必须处理");
            }finally {
                if(!password.equals("mmsszsd") || !name.equals("小卡拉")) {
                    System.out.println("剩余次数:" + count);
                    if(count == 0) {
                        System.out.println("登录失败");
                    }
                }
            }
        }
    }








    public static void loginInfo(String userName, String password){
if (!userName.equals(userName)) {
    throw new C();
}

System.out.println("登陆成功");
}





    public static void main1(String[] args) throws CloneNotSupportedException{

        A a1 = new A(5, 6);
        A a2 =(A)a1.clone();
        System.out.println(a2);







//        long l1 = System.currentTimeMillis();
//        try {
        System.out.println("aaaaaa");
            int a = -1 / 0;
        System.out.println("aaaaaa");
//        }catch (Exception e) {
//            e.printStackTrace();
//            System.out.println("请去处理！");
//        }
//        long l2 = System.currentTimeMillis();
//        System.out.println(l2 - l1);
//        l1 = System.currentTimeMillis();
//        try {
//            int a = 1 / 0;
//            System.out.println("请去处理这个异常");
//        }catch (ArithmeticException e) {
//            e.printStackTrace();
//            System.out.println("请去处理！");
//        }
//        l2 = System.currentTimeMillis();
//        System.out.println(l2 - l1);//效率很块！




    }
}
