/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-18
 * Time: 17:19
 */
public class C extends RuntimeException{ //变成闪电了
    //这里可以多一个构造方法（补充说明的 String）
    //用到的时候需要  throw手动抛出（毕竟不是现成的，是自定义的），------>[  throw new C("错误...");  ]
    //异常就是个类，里面有一个打印的方法！
    //抛出Exception异常时，默认受查异常，比较特殊
    //如果是Exception/IOException 则默认为编译异常（受查异常）（可能需要throws），即使Exception是包含两种异常的(RunTime --- IO)，
    // 了解即可，用到再说，抓大放小，相信比特！
    //如果是RunTimeException,为运行异常（非受查异常）
}
