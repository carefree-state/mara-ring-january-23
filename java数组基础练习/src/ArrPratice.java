import java.util.Arrays;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-07
 * Time: 19:25
 */
public class ArrPratice {

    public static boolean search4(int[] arr) {
        for (int i = 0; i < arr.length - 2; i++) {
            int count = 0;
            for (int j = i ; j <= i + 2; j++) {
                if(arr[j] % 2 != 1) {
                    break;
                }
                count++;
            }
            if(count == 3) {
                return true;
            }
        }
        return false;
    }
    public static void main8(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int[] array = new int[n];
        for (int i = 0; i < array.length; i++) {
            array[i] = scan.nextInt();
        }
        System.out.println(search4(array));
    }
    public static int search3(int[] arr) {
        int count = 0;
        int imax = 0;
        for (int i = 0; i < arr.length; i++) {
            count = 0;
            for (int j = 0; j < arr.length; j++) {
                if(arr[j] == arr[i]) {
                    count++;
                }
            }
            if(count > arr.length / 2) {
                imax = i;
                break;
            }
        }
        return arr[imax];
    }
    public static int findMoreNum(int[] arr) {
        Arrays.sort(arr);
        return arr[arr.length / 2];//两种极端情况下的完美重合点！
    }
    public static  int findMoreNum1(int[] arr) {
        //投票的思想去找，如果有机会就给个计数器，如果抵消了，换下一个人拿计数器，
        //这里不是选票最多，而是票数大于总人数一半，那么票数大于一半，
        // 那么就必然能拿到计数器，因为别人肯定会抵消，然后交给你
        int count = 0;
        int tmp = arr[0];
        for(int x : arr) {
            if(x == tmp) {
                count++;
            }else {
                count--;
            }
            if(count == 0) {
                tmp = x;
                count++;
            }
        }
        return tmp;
    }
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int[] array = new int[n];
        for (int i = 0; i < array.length; i++) {
            array[i] = scan.nextInt();
        }
        System.out.println("多数元素为：" + findMoreNum1(array));
    }
    public static int search2(int[] arr) {
        int ret = arr[0];
        for (int i = 1; i < arr.length; i++) {
            ret ^= arr[i];
        }
        return ret;
    }
    public static void main6(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int[] array = new int[n];
        for (int i = 0; i < array.length; i++) {
            array[i] = scan.nextInt();
        }
        System.out.println(search2(array));
    }
    public static int[] search1(int[] nums,int target) {
        int[] ret = {-1, -1};
        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j < nums.length ; j++) {
                if(nums[i] + nums[j] == target) {
                    ret[0] = i;
                    ret[1] = j;
                }
            }
        }
        return ret;
    }
    public static void main5(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int[] nums = new int[n];
        for (int i = 0; i < nums.length; i++) {
            nums[i] = scan.nextInt();
        }
        int target = scan.nextInt();
        int[] ret = search1(nums,target);
        System.out.println(Arrays.toString(ret));
    }
    public static void sort (int[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            boolean flag = true;
            for (int j = 1; j < arr.length - 1 - i; j++) {
                if(arr[j] < arr[j - 1]) {
                    int tmp = arr[j];
                    arr[j] = arr[j - 1];
                    arr[j - 1] = tmp;
                    flag = false;
                }
            }if(flag) { //说明已经排好了！
                return;
            }
        }
    }
    public static void main4(String[] args) {
        int[] array = new int[10_0000]; //冒泡很慢
        for (int i = 0; i < array.length; i++) {
            array[i] = array.length - i;
        }
        long start = System.currentTimeMillis();//获取系统时间
        sort(array);
        //Arrays.sort(array);//库方法：最快的排序方法
        long end = System.currentTimeMillis();
        //System.out.println(Arrays.toString(array));
        System.out.println(start);
        System.out.println(end - start);//差值为多少多少毫秒！
        System.out.println(end);
    }
    public static int search(int[] arr,int n) {
        int left = 0;
        int right = arr.length - 1;
        while(left <= right) {
            if (arr[(left + right) / 2] > n) {
                right = (left + right) / 2 - 1;
            } else if (arr[(left + right) / 2] < n) {
                left = (left + right) / 2 + 1;
            } else {
                return (left + right) / 2;
            }
        }
        return -(left + 1);
    }
    public static void main3(String[] args) {
        int[] array = {1, 2, 3, 5, 6, 7, 8, 9, 10 ,12};
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int ret = search(array,n);
        System.out.println("下标为 " + ret);
        System.out.println("下标为 " + Arrays.binarySearch(array, n)); //库函数，找不到返回负数 -(left +1)
    }
    public static void breakSort1(int[] array) {
        int left = 0;
        int right = array.length - 1;
        int[] tmp = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            if(array[i] % 2 == 0) {
                tmp[right--] = array[i];
            }else {
                tmp[left++] = array[i];
            }
        }
        System.arraycopy(tmp,0,array,0,array.length);
    }
    public static void  breakSort2(int[] arr) {
        int left = 0;
        int right = arr.length - 1;
        while(left < right) {
            while(left < right && arr[left] % 2 != 0) {
                left++;
            }
            while(left < right && arr[right] % 2 == 0) {
                right--;
            }
            int tmp = arr[right];
            arr[right] = arr[left];
            arr[left] = tmp;
        }
    }
    public static void main2(String[] args) {
        int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9 ,10};
        breakSort2(array);
        System.out.println(Arrays.toString(array));
    }
    public static void main1(String[] args) {
        int[] array = {1, 2, 3, 4, 5, 6, 7, 8};
        transform(array);
        System.out.println(Arrays.toString(array));
    }
    public static void transform(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            arr[i] *= 2;
        }
    }
}