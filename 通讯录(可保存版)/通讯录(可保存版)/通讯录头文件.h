#pragma once

#define _CRT_SECURE_NO_WARNINGS 1
#define NUM 3
#define PLUG 2

#include<stdio.h>
#include<string.h>
#include<assert.h>
#include<stdlib.h>
#include<errno.h>

enum INPUT
{
	EXIT,
	ADD,
	DEL,
	SEARCH,
	MODIFY,
	SHOW,
	SORT,
	SECRET,
	SHOW_SECRET,
	REMOVE_SECRET

};
enum MODI
{
	NAME = 1,
	SEX,
	AGE,
	ADDRESS,
	PHONE
};
enum op
{
	UP = 1,
	DOWN
};

struct content
{
	char name[20];
	char sex[5];
	int age;
	char address[20];
	char phone[12];
};
struct contact_list
{
	//struct content list[MAX+1];
	struct content* list;
	int sz;
	int capacity;
};

void test();

void menu1();
void menu2();
void menu3();

void init(struct contact_list* pt, char* str);

void add_man(struct contact_list* pt, struct contact_list* px);
void show_all(const struct contact_list* pt);
void del_man(struct contact_list* pt);
void search_man(const struct contact_list* pt);
void modify_man(struct contact_list* pt);
void sort_all(struct contact_list* pt);
void secret(struct contact_list* pt, struct contact_list* px);
void show_secret(const struct contact_list* pt, const struct contact_list* px);
void remove_secret(struct contact_list* pt, struct contact_list* px);
void destroy(struct contact_list* pt, struct contact_list* px);
void keep(struct contact_list* pt, char* str);

static int judge_local(struct contact_list* pt, char* Name);

void upsort(struct contact_list* pt);
void downsort(struct contact_list* pt);

int sort1(const void* e1, const void* e2);
int sort2(const void* e1, const void* e2);
int sort3(const void* e1, const void* e2);
int sort4(const void* e1, const void* e2);
int sort5(const void* e1, const void* e2);
int sort6(const void* e1, const void* e2);
int sort7(const void* e1, const void* e2);
int sort8(const void* e1, const void* e2);
int sort9(const void* e1, const void* e2);
int sort10(const void* e1, const void* e2);