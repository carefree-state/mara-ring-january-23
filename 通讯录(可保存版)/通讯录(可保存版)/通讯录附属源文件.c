#include "通讯录头文件.h"

void menu1()
{
	printf("**************************************************\n");
	printf("**********   1,add  *******  2,del（one or all）**\n");
	printf("********  3,search  *******  4,modify   **********\n");
	printf("**********  5,show  *******  6,sort    ***********\n");
	printf("********* 7,secret  *******  8,show_secret *******\n");
	printf("** 9,remove_secret  *******  0,exit and keep  ****\n");
	printf("**************************************************\n");
}
void menu2()
{
	printf("***********************************************\n");
	printf("*****  1,NAME   ******  2,SEX   ***************\n");
	printf("******  3,AGE   ******  4,ADDRESS    **********\n");
	printf("****  5,PHONE   ******  0,EXIT  ***************\n");
	printf("***********************************************\n");

}
void menu3()
{
	printf("***********************************************\n");
	printf("********   1,up    ******    2，down   ********\n");
	printf("***************    0,exit    ******************\n");
	printf("***********************************************\n");
}

void init(struct contact_list* pt, char* str)
{
	assert(pt);
	pt->sz = 0;
	//	memset(pt->list, 0, (MAX + 1) * sizeof(struct content)); 
	FILE* pfr = fopen(str, "rb");
	if (NULL == pfr)
	{
		perror("init");
		return;
	}
	int count = 0;
	fread(&count, sizeof(int), 1, pfr);
	int i = 0;
	struct content* ptr = (struct content*)malloc((count + 1) * sizeof(struct content));
	if (NULL == ptr)
	{
		perror("init");
		return;
	}
	pt->list = ptr;
	pt->capacity = count + 1;
	pt->sz = count;
	for (i = 0; i < count; i++)
	{
		fread(pt->list + i, sizeof(struct content), 1, pfr);
	}
	fclose(pfr);
	pfr = NULL;
}
void add_man(struct contact_list* pt, struct contact_list* px)
{
	printf("请输入你要添加人的信息(重名者需要加上编号):>\n");
	printf("姓名:>");
	scanf("%s", pt->list[pt->sz].name);
	printf("性别:>");
	scanf("%s", pt->list[pt->sz].sex);
	printf("地址:>");
	scanf("%s", pt->list[pt->sz].address);
	printf("手机号:>");
	scanf("%s", pt->list[pt->sz].phone);
	printf("年龄:>");
	scanf("%d", &pt->list[pt->sz].age);

	pt->sz++;
	system("cls");
	printf("添加成功\n");
	if (pt->sz == pt->capacity)
	{
		int n = (pt->capacity + PLUG) * sizeof(struct content);
		struct content* ptr = (struct content*)realloc(pt->list, n);
		if (ptr == NULL)
		{
			perror("add_man");
			return;
		}
		else
		{
			pt->list = ptr;
			pt->capacity += 2;
			ptr = (struct content*)realloc(px->list, n);
			if (ptr == NULL)
			{
				perror("add_man");
				return;
			}
			else
			{
				px->list = ptr;
				px->capacity = pt->capacity;
				px->sz = pt->sz;
			}
		}
	}
}

void show_all(const struct contact_list* pt)
{
	int ij = 0;
	for (ij = 0; ij < 60; ij++)
		printf("-");
	printf("\n");
	printf("%-20s%-5s%-20s%-12s%-3s\n", "姓名", "性别", "地址", "手机号", "年龄");
	int i = 0;
	if (pt->sz == 0)
	{
		printf("空\n");
	}
	else
	{
		for (i = 0; i < pt->sz; i++)
		{
			if (pt->list[i].name[0] != '#')
			{
				printf("%-20s%-5s%-20s%-12s%-3d\n", pt->list[i].name,
					pt->list[i].sex, pt->list[i].address,
					pt->list[i].phone, pt->list[i].age);
			}
			else
			{
				int j = 0;
				for (j = 0; j < 60; j++)
					printf("#");
				printf("\n");
			}
		}
	}
	ij = 0;
	for (ij = 0; ij < 60; ij++)
		printf("-");
	printf("\n");
}

static int judge_local(struct contact_list* pt, char* Name)
{
	int i = 0;
	for (i = 0; i < pt->sz; i++)
	{
		if (strcmp(pt->list[i].name, Name) == 0)
			return i;
	}
	return -1;
}

void del_man(struct contact_list* pt)
{
	printf("请输入密码以确保你的身份(七个字符):>");
	int count = 3;
	char str[8] = { 0 };
	while (count--)
	{
		scanf("%s", str);
		if (strcmp(str, "mmsszsd") == 0)
		{
			goto again;
		}
		printf("密码错误\n");
	}
	printf("你没有机会了\n");
	return;
again:
	show_all(pt);
	printf("请输入一个你要删除的联系人的姓名(如果全部删除请输入all):>");
	char Name[20] = { 0 };
	scanf("%s", Name);
	if (strcmp(Name, "all") == 0)
	{
		struct content* ptr = realloc(pt->list, NUM * sizeof(struct content));
		if (ptr == NULL)
		{
			perror("del_man");
			return;
		}
		else
		{
			pt->list = ptr;
			pt->sz = 0;
			pt->capacity = NUM;
			system("cls");
			printf("清除成功\n");
			return;
		}
	}
	else
	{
		int ret = judge_local(pt, Name);
		if (ret == -1)
		{
			system("cls");
			printf("查无此人\n");
			return;
		}
		else
		{
			int i = 0;
			for (i = ret; i < pt->sz; i++)
			{
				pt->list[i] = pt->list[i + 1];
			}
			pt->sz--;
			system("cls");
			printf("删除成功\n");
		}
	}
}
void search_man(const struct contact_list* pt)
{
	printf("请输入一个你要查找的联系人的姓名:>");
	char Name[20] = { 0 };
	scanf("%s", Name);
	int ret = judge_local(pt, Name);
	if (ret == -1)
	{
		system("cls");
		printf("查无此人\n");
		return;
	}
	else
	{
		system("cls");
		printf("查找成功\n");
		int ij = 0;
		for (ij = 0; ij < 60; ij++)
			printf("-");
		printf("\n");
		printf("%-20s%-5s%-20s%-12s%-3s\n", "姓名", "性别", "地址", "手机号", "年龄");
		printf("%-20s%-5s%-20s%-12s%-3d\n", pt->list[ret].name,
			pt->list[ret].sex, pt->list[ret].address,
			pt->list[ret].phone, pt->list[ret].age);
		ij = 0;
		for (ij = 0; ij < 60; ij++)
			printf("-");
		printf("\n");
	}
}
void modify_man(struct contact_list* pt)
{
	printf("请输入密码以确保你的身份(七个字符):>");
	int count = 3;
	char str[8] = { 0 };
	while (count--)
	{
		scanf("%s", str);
		if (strcmp(str, "mmsszsd") == 0)
		{
			goto again;
		}
		printf("密码错误\n");
	}
	printf("你没有机会了\n");
	return;
again:
	show_all(pt);
	printf("请输入一个你要修改的联系人的姓名:>");
	char Name[20] = { 0 };
	scanf("%s", Name);
	int ret = judge_local(pt, Name);
	if (ret == -1)
	{
		system("cls");
		printf("查无此人\n");
	}
	else
	{
		int input = 0;
		do
		{
			menu2();
			printf("请选择要修改什么:>");
			scanf("%d", &input);
			switch (input)
			{
			case NAME:
				scanf("%s", pt->list[ret].name);
				printf("修改成功\n");
				break;
			case SEX:
				scanf("%s", pt->list[ret].sex);
				printf("修改成功\n");
				break;
			case AGE:
				scanf("%d", &pt->list[ret].age);
				printf("修改成功\n");
				break;
			case ADDRESS:
				scanf("%s", pt->list[ret].address);
				printf("修改成功\n");
				break;
			case PHONE:
				scanf("%s", pt->list[ret].phone);
				printf("修改成功\n");
				break;
			case EXIT:
				system("cls");
				printf("退出成功\n");
				break;
			default:
				printf("输入失败\n");
				break;
			}
		} while (input);
	}
}
int sort1(const void* e1, const void* e2)
{
	return strcmp(((struct content*)e1)->name, ((struct content*)e2)->name);
}
int sort2(const void* e1, const void* e2)
{
	return strcmp(((struct content*)e1)->sex, ((struct content*)e2)->sex);
}
int sort3(const void* e1, const void* e2)
{
	return ((struct content*)e1)->age - ((struct content*)e2)->age;
}
int sort4(const void* e1, const void* e2)
{
	return strcmp(((struct content*)e1)->address, ((struct content*)e2)->address);
}
int sort5(const void* e1, const void* e2)
{
	return strcmp(((struct content*)e1)->phone, ((struct content*)e2)->phone);
}
int sort6(const void* e1, const void* e2)
{
	return strcmp(((struct content*)e2)->name, ((struct content*)e1)->name);
}
int sort7(const void* e1, const void* e2)
{
	return strcmp(((struct content*)e2)->sex, ((struct content*)e1)->sex);
}
int sort8(const void* e1, const void* e2)
{
	return ((struct content*)e2)->age - ((struct content*)e1)->age;
}
int sort9(const void* e1, const void* e2)
{
	return strcmp(((struct content*)e2)->address, ((struct content*)e1)->address);
}
int sort10(const void* e1, const void* e2)
{
	return strcmp(((struct content*)e2)->phone, ((struct content*)e1)->phone);
}
void upsort(struct contact_list* pt)
{
	int input = 0;
	do
	{
		menu2();
		printf("请输入要以什么为标准排序:>");

		scanf("%d", &input);
		switch (input)
		{
		case NAME:
			qsort(pt->list, pt->sz, sizeof(struct content), sort1);
			printf("排序成功\n");
			break;
		case SEX:
			qsort(pt->list, pt->sz, sizeof(struct content), sort2);
			printf("排序成功\n");
			break;
		case AGE:
			qsort(pt->list, pt->sz, sizeof(struct content), sort3);
			printf("排序成功\n");
			break;
		case ADDRESS:
			qsort(pt->list, pt->sz, sizeof(struct content), sort4);
			printf("排序成功\n");
			break;
		case PHONE:
			qsort(pt->list, pt->sz, sizeof(struct content), sort5);
			printf("排序成功\n");
			break;
		case EXIT:
			printf("退出成功\n");
			break;
		default:
			printf("输入失败\n");
			break;
		}
	} while (input<EXIT || input>PHONE);
}
void downsort(struct contact_list* pt)
{
	int input = 0;
	do
	{
		menu2();
		printf("请输入要以什么为标准排序:>");

		scanf("%d", &input);
		switch (input)
		{
		case NAME:
			qsort(pt->list, pt->sz, sizeof(struct content), sort6);
			printf("排序成功\n");
			break;
		case SEX:
			qsort(pt->list, pt->sz, sizeof(struct content), sort7);
			printf("排序成功\n");
			break;
		case AGE:
			qsort(pt->list, pt->sz, sizeof(struct content), sort8);
			printf("排序成功\n");
			break;
		case ADDRESS:
			qsort(pt->list, pt->sz, sizeof(struct content), sort9);
			printf("排序成功\n");
			break;
		case PHONE:
			qsort(pt->list, pt->sz, sizeof(struct content), sort10);
			printf("排序成功\n");
			break;
		case EXIT:
			printf("退出成功\n");
			break;
		default:
			printf("输入失败\n");
			break;
		}
	} while (input<EXIT || input>PHONE);
}
void sort_all(struct contact_list* pt)
{
	int option = 0;
	do
	{
		menu3();
		printf("请输入要以什么规律排序:>");
		scanf("%d", &option);
		switch (option)
		{
		case UP:
			upsort(pt);
			break;
		case DOWN:
			downsort(pt);
			break;
		case EXIT:
			printf("退出成功\n");
			break;
		default:
			printf("输入失败\n");
			break;
		}
	} while (option<EXIT || option>DOWN);
}
void secret(struct contact_list* pt, struct contact_list* px)
{
	assert(px);
	printf("请输入密码以确保你的身份(七个字符):>");
	int count = 3;
	char str[8] = { 0 };
	while (count--)
	{
		scanf("%s", str);
		if (strcmp(str, "mmsszsd") == 0)
		{
			goto again;
		}
		printf("密码错误\n");
	}
	printf("你没有机会了\n");
	return;
again:
	show_all(pt);
	printf("请输入你要码掉的人的姓名:>");
	char Name[20] = { 0 };
	scanf("%s", Name);
	int ret = judge_local(pt, Name);
	if (ret == -1)
	{
		system("cls");
		printf("查无此人\n");
		return;
	}
	else
	{
		memcpy(px->list + ret, pt->list + ret, sizeof(struct content));
		memset(pt->list + ret, '#', sizeof(struct content));
	}
	printf("保密成功\n");
}
void show_secret(const struct contact_list* pt, const struct contact_list* px)
{
	printf("请输入密码以确保你的身份(七个字符):>");
	int count = 3;
	int ij = 0;
	char str[8] = { 0 };
	while (count--)
	{
		scanf("%s", str);
		if (strcmp(str, "mmsszsd") == 0)
		{
			goto again;
		}
		printf("密码错误\n");
	}
	printf("你没有机会了\n");
	return;
again:
	for (ij = 0; ij < 60; ij++)
		printf("-");
	printf("\n");
	printf("%-20s%-5s%-20s%-12s%-3s\n", "姓名", "性别", "地址", "手机号", "年龄");
	int i = 0;
	if (pt->sz == 0)
	{
		printf("空\n");
	}
	else
	{
		for (i = 0; i < pt->sz; i++)
		{
			if (pt->list[i].name[0] != '#')
			{
				printf("%-20s%-5s%-20s%-12s%-3d\n", pt->list[i].name,
					pt->list[i].sex, pt->list[i].address,
					pt->list[i].phone, pt->list[i].age);
			}
			else
			{
				printf("%-20s%-5s%-20s%-12s%-3d\n", px->list[i].name,
					px->list[i].sex, px->list[i].address,
					px->list[i].phone, px->list[i].age);
			}
		}
		ij = 0;
		for (ij = 0; ij < 60; ij++)
			printf("-");
		printf("\n");
		printf("展示成功\n");
	}
}
void remove_secret(struct contact_list* pt, struct contact_list* px)
{
	printf("请输入密码以确保你的身份(七个字符):>");
	int count = 3;
	char str[8] = { 0 };
	while (count--)
	{
		scanf("%s", str);
		if (strcmp(str, "mmsszsd") == 0)
		{
			goto again;
		}
		else
			printf("密码错误\n");
	}
	printf("你没有机会了\n");
	return;
again:
	show_all(pt);
	printf("你要揭晓的序号（从1开始由上往下数）:>");
	int n = 0;
	scanf("%d", &n);
	if (pt->list[n - 1].name[0] == '#')
		memmove(&pt->list[n - 1], &px->list[n - 1], sizeof(struct content));
	show_all(pt);
	printf("揭晓成功\n");
}
void destroy(struct contact_list* pt, struct contact_list* px)
{
	free(pt->list);
	free(px->list);
	pt->sz = 0;
	px->sz = 0;
	pt->capacity = 0;
	px->capacity = 0;
}
void keep(const struct contact_list* pt, char* str)
{
	FILE* pf = fopen(str, "wb");
	if (NULL == pf)
	{
		perror("保存失败");
		return;
	}
	int i = 0;
	fwrite(&pt->sz, sizeof(int), 1, pf);
	for (i = 0; i < pt->sz; i++)
	{
		fwrite(pt->list + i, sizeof(struct content), 1, pf);
	}
	fclose(pf);
	pf = NULL;
}
void test()
{
	int input = 0;
	struct contact_list total;
	struct contact_list extra;
	init(&total, "Contact1.txt");
	init(&extra, "Contact2.txt");
	do
	{
		menu1();
		printf("请做出选择:>");
		scanf("%d", &input);
		system("cls");
		switch (input)
		{
		case ADD:
			add_man(&total, &extra);
			break;
		case DEL:
			del_man(&total);
			break;
		case SEARCH:
			search_man(&total);
			break;
		case MODIFY:
			modify_man(&total);
			break;
		case SHOW:
			show_all(&total);
			printf("展示成功\n");
			break;
		case SORT:
			sort_all(&total);
			break;
		case EXIT:
			keep(&total, "Contact1.txt");
			keep(&extra, "Contact2.txt");
			destroy(&total, &extra);
			printf("退出成功\n");
			break;
		case SECRET:
			secret(&total, &extra);
			break;
		case SHOW_SECRET:
			show_secret(&total, &extra);
			break;
		case REMOVE_SECRET:
			remove_secret(&total, &extra);
			break;
		default:
			printf("输入错误\n");
			break;
		}
	} while (input);
}