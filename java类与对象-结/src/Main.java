/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-11
 * Time: 1:25
 */
import java.util.Scanner;

public class Main {


    public static void main(String[] args) {
        Data data = new Data(1,2);
        System.out.println(data);
        System.out.println(data);
    }
    public static void main1(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextInt()) {
            Data.z++;
            System.out.println("第" + Data.z + "次");
            int x = scanner.nextInt();
            int y = scanner.nextInt();
            Data data = new Data(x, y);
            System.out.println(data.getX() + data.getY());
        }
    }

}

class Data {


    private int x;
    private int y;
    static int  z = 0;
    public Data(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "Data{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    //接口，用这个才能访问对象
    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}