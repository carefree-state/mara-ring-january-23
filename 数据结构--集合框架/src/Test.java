import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-19
 * Time: 16:53
 */

class MyArr <T> {
    T[] ts = (T[]) new Object[4];

    public T getTs(int pos) {
        return ts[pos];
    }

    public void setTs(int pos, T t) {
        this.ts[pos] = t;
    }
}
public class Test {
    public static void main(String[] args) {
        MyArr<Integer>[] myArr = new MyArr<Integer>[5];    //不能写基本数据类型，不会自动装箱
    }


    public static void main2(String[] args) {
        Object obj = new Object();//
        // Object的高级代码不需要理解，直接用就好
        obj = 5.66;//double --> Double(自动装包)  这种向上转型不会影响到一些东西的

        System.out.println(obj);//可以打印，但是无法在不进行强制类型转化的情况下完成获取信息
        //obj接收什么就打印什么
    }
    public static void main1(String[] args) {
        /*int a = Integer.valueOf("111");
        int b = Integer.parseInt("111");
        Integer c = Integer.valueOf(111);
        System.out.println(Integer.valueOf("x", 36));//2<=radix<=36
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);*/

        int[] nums = {0, 1, 7, 3, 5, 6, 2};
        Arrays.sort(nums);
        int right = nums.length - 1;
        int left = 0;
        while(left < right) {
            int flag = right + left;
            if(nums[flag / 2] == flag / 2) {
                if(left == flag / 2) {
                    break;
                }
                left = flag / 2;
            }else if(nums[flag / 2] > flag / 2) {
                if(right == flag / 2) {
                    break;
                }
                right = flag / 2;
            }
        }

        System.out.println(right);
    }
}
