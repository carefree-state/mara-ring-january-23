package opera;

import book.BookList;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-15
 * Time: 13:42
 */
public class Search implements IOpera{
    public static int search(BookList bookList, String name) {
        for (int i = 0; i < bookList.getNumber(); i++) {
            if(bookList.getBooks()[i].getName().equals(name)) {
                return i;
            }
        }
        return -1;
    }
    @Override
    public void work(BookList bookList, String person) {
        System.out.print("请输入你想查询的书:>");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();
        int index = search(bookList, name);
        if(index != -1) {
            System.out.println("找到了");
            System.out.println(bookList.getBooks()[index]);
        }else {
            System.out.println("未能找到");
        }
    }
}
