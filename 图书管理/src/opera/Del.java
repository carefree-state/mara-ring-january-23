package opera;

import book.BookList;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-15
 * Time: 13:40
 */
public class Del implements IOpera{
    @Override
    public void work(BookList bookList,  String person) {
        System.out.print("请输入你要删除的书:>");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();
        int index = Search.search(bookList, name);
        if(index != -1) {
            if(bookList.getBooks(index).isState()) {
                System.out.println("此书被借出，无法删除");
                System.out.println("请找" + bookList.getBooks(index).getBorrower() + "要回此书");
                return;
            }
            System.out.println("删除成功");
            bookList.setBooks(null, index);
            for (int i = index + 1; i < bookList.getNumber(); i++) {
                bookList.setBooks(bookList.getBooks(i), i - 1);
                bookList.setBooks(null, i);
            }
            bookList.setNumber(bookList.getNumber() - 1);
        }else {
            System.out.println("找不到你要删除的书");
        }
    }
}
