package opera;

import book.BookList;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-15
 * Time: 13:43
 */
public class Return implements IOpera{
    @Override
    public void work(BookList bookList, String person) {
        System.out.print("请输入你要归还的书:>");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();
        int index = Search.search(bookList, name);
        if(index != -1 && bookList.getBooks(index).isState()) {
            System.out.println("归还成功");
            bookList.getBooks(index).setState(false);
        }else {
            System.out.println("此书并不是你在此处借的");
        }
    }
}
