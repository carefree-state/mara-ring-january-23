package opera;

import book.BookList;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-15
 * Time: 13:42
 */
public class Exit implements IOpera{
    @Override
    public void work(BookList bookList, String person) {
        System.out.println("退出成功");
        //System.exit(0) 为正常退出执行！就是直接不执行了，而不是跳到末尾，也不是直接此方法结束，是整个程序结束
    }
}
