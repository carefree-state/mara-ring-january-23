package opera;

import book.Book;
import book.BookList;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-15
 * Time: 13:40
 */
public class Add implements IOpera{
    @Override
    public void work(BookList bookList, String s) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("请输入要添加的书名:>");
        String name = scanner.nextLine();
        System.out.print("请输入这本书的作者:>");
        String author = scanner.nextLine();
        System.out.print("请输入这本书的类别:>");
        String type = scanner.nextLine();
        System.out.print("请输入这本书的价格:>");
        Double price = scanner.nextDouble();
        Book book = new Book(name, author, price, type);
        bookList.setBooks(book, bookList.getNumber());
        bookList.setNumber(bookList.getNumber() + 1);
        if(bookList.getCapacity() == bookList.getNumber()) {
            bookList.setCapacity(bookList.getCapacity() + 5);
            Book[] books = Arrays.copyOf(bookList.getBooks(), bookList.getCapacity());
            bookList.setBooks(books);
        }
        System.out.println("添加成功！");
    }
}
