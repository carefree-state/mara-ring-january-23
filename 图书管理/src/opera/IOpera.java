package opera;

import book.BookList;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-15
 * Time: 13:46
 */
public interface IOpera {
    public void work(BookList bookList,  String person);
}
