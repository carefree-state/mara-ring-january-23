package opera;

import book.BookList;
import user.User;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-15
 * Time: 13:40
 */
public class Borrow implements IOpera{
    @Override
    public void work(BookList bookList, String person) {
        System.out.print("请输入你要借的书:>");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();
        int index = Search.search(bookList, name);
        if(index != -1) {
            if(bookList.getBooks(index).isState()) {
                System.out.println(name + "已被借走");
            }else {
                System.out.println("借阅成功");
                bookList.getBooks(index).setState(true);
                bookList.getBooks(index).setBorrower(person);
            }
        }else {
            System.out.println("你要的书这里找不到");
        }
    }
}
