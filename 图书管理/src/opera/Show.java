package opera;

import book.BookList;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-15
 * Time: 13:43
 */
public class Show implements IOpera{
    @Override
    public void work(BookList bookList, String person) {
        System.out.println(bookList);
        System.out.println("展示成功");
    }
}
