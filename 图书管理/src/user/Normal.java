package user;

import book.BookList;
import opera.*;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-15
 * Time: 13:49
 */
public class Normal extends User{
    public Normal(String name) {
        super(name);
    }
    private IOpera[] iOperas = {
            new Exit(),
            new Search(),
            new Borrow(),
            new Return()
    };
    @Override
    public void menu(BookList bookList) {
        int choice = 0;
        do {
            System.out.println("********************************");
            System.out.println("hello " + name);
            System.out.println("** 1. 查阅图书 ");
            System.out.println("** 2. 借阅图书 ");
            System.out.println("** 3. 归还图书 ");
            System.out.println("** 0. 退出 ");
            System.out.println("********************************");
            System.out.print("请输入你的选择:>");
            Scanner scanner = new Scanner(System.in);
            choice = scanner.nextInt();
            iOperas[choice].work(bookList, this.name);
        }while(!(iOperas[choice] instanceof  Exit));
    }
}
