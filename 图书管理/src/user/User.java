package user;

import book.BookList;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-15
 * Time: 13:47
 */
public abstract class User {
    protected String name;
    public abstract void menu(BookList bookList);
    public User(String name) {
        this.name = name;
    }
}
