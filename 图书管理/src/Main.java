import book.BookList;
import user.Admin;
import user.Normal;
import user.User;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-15
 * Time: 13:50
 */
public class Main {
    public static User judge(int role, String name) {
        return role == 1 ? new Admin(name) : new Normal(name);
    }
    public static void main(String[] args) {
        BookList bookList = new BookList();
        while(true) {
            Scanner scanner = new Scanner(System.in);
            System.out.print("请输入你的名字:>");
            String name = scanner.nextLine();
            System.out.println("1. 管理员\n2. 普通用户");
            System.out.print("请输入你的身份:>");
            int role = scanner.nextInt();
            User user = judge(role, name);
            user.menu(bookList);
        }
    }
}
