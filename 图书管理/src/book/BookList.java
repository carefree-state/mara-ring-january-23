package book;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-15
 * Time: 13:39
 */
public class BookList {
    private Book[] books = new Book[5];
    private int number = 3;
    private int capacity = 5;
    {
        books[0] = new Book("小王子1", "王子1", 99.9, "学习");
        books[1] = new Book("小王子2", "王子2", 99.9, "学习");
        books[2] = new Book("小王子3", "王子3", 99.9, "学习");
    }

    public void setBooks(Book[] books) {
        this.books = books;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    @Override
    public String toString() {
        String str = "";
        for (int i = 0; i < number; i++) {
            str += "\n" + books[i];
        }
        return str + '\n';
    }

    public Book[] getBooks() {
        return books;
    }
    public Book getBooks(int pos) {
        return books[pos];
    }

    public void setBooks(Book book, int pos) {
        this.books[pos] = book;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

}
