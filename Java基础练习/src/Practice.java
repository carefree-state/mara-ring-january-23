import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-06
 * Time: 12:37
 */
public class Practice {

    public static int maX(int m ,int n) {
        return  m > n ? m : n;
    }
    public static double maX(double m ,double n) {
        return  m > n ? m : n;
    }
    public static double maX(double a,double b,double c) {
        return maX(a,b) > c ? maX(a,b) : c ;
    }
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int m = scan.nextInt();
        System.out.println(maX(n,m));
        double a = scan.nextDouble();
        double b = scan.nextDouble();
        double c = scan.nextDouble();
        System.out.println(maX(a,b,c));
    }
    public static int sum(int m,int n) {
        return m + n;
    }
    public static double sum(double a,double b,double c) {
        return a + b + c;
    }
    public static void main17(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int m = scan.nextInt();
        System.out.println(sum(n,m));
        double a = scan.nextDouble();
        double b = scan.nextDouble();
        double c = scan.nextDouble();
        System.out.println(sum(a,b,c));
    }
    public static int fic(int n) {
        int a = 1;
        int b = 1;
        for(int i = 3; i <= n; i++) {
            int tmp = b;
            b += a;
            a = tmp;
        }
        return b;
    }
    public static void main16(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        System.out.println(fic(n));
    }
    public static int max2(int m, int n) {
        return m > n ? m : n;
    }
    public static int max3(int m ,int n ,int p) {
        return max2(m , n) > p ? max2(m , n) : p;
    }
    public static void main15(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int m = scan.nextInt();
        int p = scan.nextInt();
        System.out.println(max3(n, m, p));
    }
    public static void nList(int n) {
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print(i + "×" + j + "=" + i * j + "  ");
            }
            System.out.println();
        }
    }
    public static void main14(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        nList(n);
    }
        public static void logOn() {
            Scanner scan = new Scanner(System.in);
            int count = 0;
            do{
                System.out.print("请输入:> ");
                count++;
                String str = scan.nextLine();
                //字符串比较用equals！！！  (str1).(str2)
                if("12345".equals(str)) {
                    System.out.println("恭喜你登录成功！");
                    break;
                }else {
                    System.out.println("密码错误！");
                    if(3 == count) {
                        System.out.println("拜拜，机会已用完！");
                    }
                }
            }while(count < 3);
        }
    public static void main13(String[] args) {
        logOn();
    }
    public static void breakUp(int n) {
        if(n == 0) {
            System.out.println(0);
        }
        while(0 != n) {
            System.out.print(n % 10 + " ");
            n /= 10;
        }
    }
    public static void main12(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        breakUp(n);
    }
    public static double revNumSum(int n) {
        int flag = 1;
        double sum = 0;
        for(int i = 1; i <= n; i++) {
            sum += flag * 1.0 / i;
            flag = -flag;
        }
        return sum;
    }
    public static void main11(String[] args){
        System.out.print("请输入一个数字:>");
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        System.out.println(revNumSum(n));
    }
    public static int facNum(int num) {
        int sum = 0;
        for(int i = 1; i < num; i++) {
            sum += fac(i);
        }
        return sum;
    }
    //方法的名字可以一样！，构成方法的重载，但是”不能完全“（参数列表，返回值无所谓，可同可不同）一样！！！
    //一词多义
    public static short facNum(short num) {
        short sum = 0;
        for(int i = 1; i < num; i++) {
            sum += fac(i);
        }
        return sum;
    }
    public static int fac(int num) {
        int ret = 1;
        for(int i = 1; i <= num; i++) {
            ret *= i;
        }
        return ret;
    }
    public static void main10(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        String str1 = "";
        String str2 = " ";
        for(int i = 0; i < 32; i++) {
            switch(i % 2){
                case 0:
                    if(((n >> i) & 1) == 1) {
                    str1 = "1 " + str1;
                    }else {
                    str1 = "0 " + str1;
                    }
                    break;
                case 1:
                    if(((n >> i) & 1) == 1) {
                        str2 = "1 " + str2;
                    }else {
                        str2 = "0 " + str2;
                    }
                    break;
                default:
                    System.out.println("出错了");
                    break;
            }
        }
        System.out.println();//什么都不输入等于只输出换行符
        System.out.println("奇数位：" + str1);
        System.out.println("偶数位：" + str2);
    }
//自幂数
    public static void main9(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        for(int i = 0; i <= n; i++) {
            int tmp = i;
            int sum = 0;
            int count = 0;
            if(i == 0) {
                count++;
            }
            while(0 != tmp) {
               count++;
                tmp /= 10;
            }
            tmp = i;
            while(0 != tmp) {
                sum += Math.pow(tmp % 10,count);
                tmp /= 10;
            }
            if(sum == i) {
                System.out.println(sum);
            }
        }
    }
    //九九乘法表
    public static void main8(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        for(int i = 1; i <= n; i++) {
             for(int j = 1; j <= i; j++) {
                 System.out.print(i + "×" + j + "=" + i * j +"  ");
             }
            System.out.print('\n');
        }
    }
    public static void main7(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNextInt()) { // 注意 while 处理多个 case
            int a = in.nextInt();
            for(int i = 0; i < a; i++) {
                for(int j = 0; j < a; j++) {
                    if(i == j || i + j == a - 1) {
                        System.out.print("*");
                    }else {
                        System.out.print(" ");
                    }
                }
                System.out.print('\n');
            }

        }
    }
    public static void main6(String[] args) {
        System.out.print("请输入两个数字:>");
        Scanner scan = new Scanner(System.in);
        int n1 = scan.nextInt();
        int n2 = scan.nextInt();
        while(0 != n1 % n2) {
            int tmp = n2;
            n2 = n1 % n2;
            n1 = tmp;
        }
        System.out.println("最大公约数为：" + n2);
    }
    public static void main5(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("请输入一个数字:>");
        int n = scan.nextInt();
        int num = 0;
        while(n != 0) {
            num++;
            n &= n-1;
        }
        System.out.println(num);
    }
    public static void main4(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("请输入一个数字来判断！");
        int n = scan.nextInt();
        if(Judge(n)) {
            System.out.println("yes");
        }else {
            System.out.println("no");
        }
    }
    public static boolean Judge(int n) {
        int i = 0;
        for(i = 2; i <= Math.sqrt(n); i++) {   //Math.sqrt()表示根号
            if(n % i == 0) {
                break;
            }
        }
        if(i > n/2) {
            return false;
        }else {
            return true;
        }

    }
    public static void main3(String[] args) {
        for(int i = 1000; i <= 2000; i++) {
            if(i % 400 == 0 || (i % 4 ==0 && i % 100 != 0)) {
                System.out.print(i + " ");
            }
        }
    }
    public static void main2(String[] args) {
        int num = 0;
        for(int i = 1; i <= 100; i++) {
            int tmp = i;
            while (tmp != 0) {
                if(tmp % 10 == 9) {
                    num++;
                }
                tmp /= 10;
            }
        }
        System.out.println(num);
    }
    public static void main1(String[] args) {
        for(int i = 2; i <= 100; i++) {
            if(Judge(i)){
                System.out.println(i);
            }
        }
    }
}