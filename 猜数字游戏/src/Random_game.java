import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

import java.util.Random;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-06
 * Time: 2:52
 */
public class Random_game {
    public static void  menu() {
        System.out.println("****************************");
        System.out.println("********  1. start  ********");
        System.out.println("********  0. exit   ********");
        System.out.println("****************************");
        System.out.print("请选择一个数字:>");
    }
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Random random = new Random();
        int input = 0;
        int randNum = random.nextInt(100);//[0-100) ---> (50) +50 ---> [50,100)
        do{
            menu();
            input = scan.nextInt();
            switch (input){
                case 1:
                    while(true) {
                        System.out.print("猜数字:>");
                        int num = scan.nextInt();
                        if(num > randNum) {
                            System.out.println("猜大了捏");
                        }else if(num < randNum) {
                            System.out.println("猜小了捏");
                        }else {
                            System.out.println("猜中了捏");
                            break;
                        }
                    }
                    break;
                case 0:
                    System.out.println("退出成功");
                    break;
                default:
                    System.out.println("输入失败");
                    break;
            }
        }while(input != 0);
    }
}
