/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 20404
 * Date: 2023-01-12
 * Time: 11:29
 */
class Dog {
    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    int a = 5;

}

public class Inherit extends Dog{
    static int cnt = 6;
    int a = 1;
    static{
        cnt += 9;
    }
    {
        this.a = 0;//普通代码块
        System.out.println("小卡拉");
    }

    public static void main(String[] args) {
        //为什么static代码块只运行一次，因为它在main执行之前就运行了！！！而其他的是在构造引用时运行（普通代码块和构造方法）
        System.out.println("cnt = " + cnt);
        Inherit inherit = new Inherit();
        System.out.println(inherit.getA());

    }
    static{
        cnt /=3;
    };
}